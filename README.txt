# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
# Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

 ---------------------------------------------------------
| Dungeon RPG SFML https://gitorious.org/dungeon-rpg-sfml |
 ---------------------------------------------------------

Skeleton to start a new project that uses SFML http://www.sfml-dev.org/


 -----------------------------------------
| System requirement for Dungeon RPG SFML |
 -----------------------------------------

The required packages are:
build-essential cmake libglew-dev libx11-dev libxext-dev libgl-dev libxrandr-dev libfreetype6-dev libjpeg-dev libsndfile-dev libopenal-dev

To install all dependencies, you can download and execute:
https://gitorious.org/install_project/install_project/source/master:example/install_project_SFML_2.1.py
https://gitorious.org/install_project/install_project/source/master:example/install_project_hnc.py
from https://gitorious.org/install_project

You also need to install Thōth:
https://gitorious.org/thoth

 -------------------------------------
| Compilation of New Dungeon RPG SFML |
 -------------------------------------

Use CMake to build Dungeon RPG SFML:
---------------------------------------
mkdir build
cd build
cmake .. #-DCMAKE_INSTALL_PREFIX=/path/to/install/the/project
make

Run the executable:
------------------
./main
