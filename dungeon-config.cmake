# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the Dungeon RPG SFML library

# DUNGEON_FOUND       - System has Dungeon RPG SFML lib
# THOTH_INCLUDE_DIR - The Dungeon RPG SFML include directory


if (DUNGEON_INCLUDE_DIR)
	# Already in cache, be silent
	set(DUNGEON_FIND_QUIETLY TRUE)
endif()

find_path(DUNGEON_INCLUDE_DIR NAMES dungeon.hpp)

if (DUNGEON_INCLUDE_DIR)
	message(STATUS "Header dungeon.hpp found =) ${DUNGEON_INCLUDE_DIR}")
else()
	message(STATUS "Header dungeon.hpp not found :(")
endif()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(DUNGEON DEFAULT_MSG DUNGEON_INCLUDE_DIR)

mark_as_advanced(DUNGEON_INCLUDE_DIR)
