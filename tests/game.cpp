// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <TGUI/TGUI.hpp>

#include <dungeon/game.hpp>
#include <dungeon/menu.hpp>

int main()
{
	auto const & mouse_cursor = thoth::textures().load_extern("cursor", "../media/texture/Lancel/Knights_Glove_Mouse_Cursor/RPG_Mouse_Cursor_3_0.png");
	dungeon::game g(mouse_cursor);
	sf::Image icon;
    icon.loadFromFile("../media/texture/ForeverDarkWoods/icon.png");
    g.window().setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	dungeon::menu::main_menu(g);
	
	return 0;
} 
