// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <thoth/textures.hpp>
#include <thoth/sprite.hpp>
#include <thoth/collision.hpp>

#include <dungeon/misc.hpp>
#include <dungeon/map.hpp>
#include <dungeon/hero.hpp>
#include <dungeon/camera.hpp>
#include <dungeon/placard.hpp>
#include <dungeon/setting.hpp>
#include <dungeon/combat.hpp>

int main() 
{
	auto const & mouse_cursor = thoth::textures().load_extern("cursor", "../media/texture/Lancel/Knights_Glove_Mouse_Cursor/RPG_Mouse_Cursor_3_0.png");
	
	dungeon::setting setting(mouse_cursor);
	
	sf::RenderWindow window(sf::VideoMode(1024,768), "Gameplay");
	window.setMouseCursorVisible(false);
	setting.set_size_window(sf::Vector2u(1024, 768));
	
	// Background Map
	auto const & map_room = thoth::textures().load_extern("room", "../media/texture/Dungeon/map/demo.png");
	dungeon::map map(map_room, 3840.f, 896.f);

	auto const & misc_bridge_1 = thoth::textures().load_extern("bridge_1", "../media/texture/Dungeon/misc/bridge_1.png");
	dungeon::misc b1(misc_bridge_1);
	b1.m_sprite.set_position(605,550);
	map.add_static(b1);
	
	auto const & rune1 = thoth::textures().load_extern("rune1", "../media/texture/Clint_Bellanger/r1.png");
	auto const & rune2 = thoth::textures().load_extern("rune2", "../media/texture/Clint_Bellanger/r2.png");
	auto const & rune3 = thoth::textures().load_extern("rune3", "../media/texture/Clint_Bellanger/r3.png");
	auto const & rune4 = thoth::textures().load_extern("rune4", "../media/texture/Clint_Bellanger/r4.png");
	
	/// Teleporter
	sf::SoundBuffer buffer;
	buffer.loadFromFile("../media/sound/Ogrebane/teleport.wav");
	sf::Sound teleport;
	teleport.setBuffer(buffer);
	teleport.setVolume(100);
	
	dungeon::teleporter tp1(rune1);
	tp1.m_anim.set_position(1770, 552);
	tp1.m_anim.add("a", 0.2f, { rune1, rune2, rune3, rune4 });
	map.add_teleporter(tp1);
	
	dungeon::teleporter tp2(rune1);
	tp2.m_anim.set_position(1990, 552);
	tp2.m_anim.add("a", 0.2f, { rune1, rune2, rune3, rune4 });
	map.add_teleporter(tp2);
	
	dungeon::teleporter tp3;
	tp3.m_sprite.set_position(2448, 490);
	map.add_teleporter(tp3);
	
	/// House
	auto const & house = thoth::textures().load_extern("house", "../media/texture/Dungeon/house/house_2.png");
	dungeon::misc h1(house);
	h1.m_sprite.set_position(2485, 430);
	map.add(h1);
	
	/// Blocks
	dungeon::misc bl1;
	bl1.m_sprite.set_position(640,880);
	map.add_block(bl1);
	
	dungeon::misc bl2;
	bl2.m_sprite.set_position(575,880);
	map.add_block(bl2);
	
	dungeon::misc bl3;
	bl3.m_sprite.set_position(605,880);
	map.add_block(bl3);
	
	dungeon::misc bl4;
	bl4.m_sprite.set_position(575,755);
	map.add_block(bl4);
	
	dungeon::misc bl5;
	bl5.m_sprite.set_position(640,755);
	map.add_block(bl5);
	
	dungeon::misc bl6;
	bl6.m_sprite.set_position(575, 790);
	map.add_block(bl6);
	
	dungeon::misc bl7;
	bl7.m_sprite.set_position(605, 790);
	map.add_block(bl7);
	
	dungeon::misc bl8;
	bl8.m_sprite.set_position(640, 790);
	map.add_block(bl8);
	
	dungeon::misc bl9;
	bl9.m_sprite.set_position(575, 790);
	map.add_block(bl9);
	
	dungeon::misc bl10;
	bl10.m_sprite.set_position(575, 725);
	map.add_block(bl10);
	
	dungeon::misc bl11;
	bl11.m_sprite.set_position(640, 725);
	map.add_block(bl11);
	
	dungeon::misc bl12;
	bl12.m_sprite.set_position(640, 690);
	map.add_block(bl12);
	
	dungeon::misc bl13;
	bl13.m_sprite.set_position(575, 690);
	map.add_block(bl13);
	
	dungeon::misc bl14;
	bl14.m_sprite.set_position(575, 655);
	map.add_block(bl14);
	
	dungeon::misc bl15;
	bl15.m_sprite.set_position(640, 655);
	map.add_block(bl15);
	
	dungeon::misc bl16;
	bl16.m_sprite.set_position(640, 615);
	map.add_block(bl16);
	
	dungeon::misc bl17;
	bl17.m_sprite.set_position(575, 615);
	map.add_block(bl17);
	
	dungeon::misc bl18;
	bl18.m_sprite.set_position(575, 590);
	map.add_block(bl18);
	
	dungeon::misc bl19;
	bl19.m_sprite.set_position(605, 590);
	map.add_block(bl19);
	
	dungeon::misc bl20;
	bl20.m_sprite.set_position(640, 590);
	map.add_block(bl20);
	
	dungeon::misc bl21;
	bl21.m_sprite.set_position(640, 500);
	map.add_block(bl21);
	
	dungeon::misc bl22;
	bl22.m_sprite.set_position(605, 500);
	map.add_block(bl22);
	
	dungeon::misc bl23;
	bl23.m_sprite.set_position(575, 500);
	map.add_block(bl23);
	
	dungeon::misc bl24;
	bl24.m_sprite.set_position(640, 495);
	map.add_block(bl24);
	
	dungeon::misc bl25;
	bl25.m_sprite.set_position(575, 495);
	map.add_block(bl25);
	
	dungeon::misc bl26;
	bl26.m_sprite.set_position(640, 465);
	map.add_block(bl26);
	
	dungeon::misc bl27;
	bl27.m_sprite.set_position(545, 465);
	map.add_block(bl27);
	
	dungeon::misc bl28;
	bl28.m_sprite.set_position(575, 465);
	map.add_block(bl28);
	
	dungeon::misc bl29;
	bl29.m_sprite.set_position(640, 425);
	map.add_block(bl29);
	
	dungeon::misc bl30;
	bl30.m_sprite.set_position(505, 425);
	map.add_block(bl30);
	
	dungeon::misc bl31;
	bl31.m_sprite.set_position(575,822);
	map.add_block(bl31);
	
	dungeon::misc bl32;
	bl32.m_sprite.set_position(575,847);
	map.add_block(bl32);
	
	dungeon::misc bl33;
	bl33.m_sprite.set_position(640,847);
	map.add_block(bl33);
	
	dungeon::misc bl34;
	bl34.m_sprite.set_position(640,822);
	map.add_block(bl34);
	
	dungeon::misc bl35;
	bl35.m_sprite.set_position(466,391);
	map.add_block(bl35);
	
	dungeon::misc bl36;
	bl36.m_sprite.set_position(432, 391);
	map.add_block(bl36);
	
	dungeon::misc bl37;
	bl37.m_sprite.set_position(404, 391);
	map.add_block(bl37);
	
	dungeon::misc bl38;
	bl8.m_sprite.set_position(378, 379);
	map.add_block(bl38);
	
	dungeon::misc bl39;
	bl39.m_sprite.set_position(378, 364);
	map.add_block(bl39);
	
	dungeon::misc bl40;
	bl40.m_sprite.set_position(380, 337);
	map.add_block(bl40);
	
	dungeon::misc bl41;
	bl41.m_sprite.set_position(679, 360);
	map.add_block(bl41);
	
	dungeon::misc bl42;
	bl42.m_sprite.set_position(670, 340);
	map.add_block(bl42);
	
	dungeon::misc bl43;
	bl43.m_sprite.set_position(659, 392);
	map.add_block(bl43);
	
	/// RIVE 2
	dungeon::misc bl44;
	bl44.m_sprite.set_position(640 + 1280,880);
	map.add_block(bl44);
	
	dungeon::misc bl45;
	bl45.m_sprite.set_position(575 + 1280,880);
	map.add_block(bl45);
	
	dungeon::misc bl46;
	bl46.m_sprite.set_position(605 + 1280,880);
	map.add_block(bl46);
	
	dungeon::misc bl47;
	bl47.m_sprite.set_position(575 + 1280,755);
	map.add_block(bl47);
	
	dungeon::misc bl48;
	bl48.m_sprite.set_position(640 + 1280,755);
	map.add_block(bl48);
	
	dungeon::misc bl49;
	bl49.m_sprite.set_position(575 + 1280, 790);
	map.add_block(bl49);
	
	dungeon::misc bl50;
	bl50.m_sprite.set_position(605 + 1280, 790);
	map.add_block(bl50);
	
	dungeon::misc bl51;
	bl51.m_sprite.set_position(640 + 1280, 790);
	map.add_block(bl51);
	
	dungeon::misc bl52;
	bl52.m_sprite.set_position(575 + 1280, 790);
	map.add_block(bl52);
	
	dungeon::misc bl53;
	bl53.m_sprite.set_position(575 + 1280, 725);
	map.add_block(bl53);
	
	dungeon::misc bl54;
	bl54.m_sprite.set_position(640 + 1280, 725);
	map.add_block(bl54);
	
	dungeon::misc bl55;
	bl55.m_sprite.set_position(640 + 1280, 690);
	map.add_block(bl55);
	
	dungeon::misc bl56;
	bl56.m_sprite.set_position(575 + 1280, 690);
	map.add_block(bl56);
	
	dungeon::misc bl57;
	bl57.m_sprite.set_position(575 + 1280, 655);
	map.add_block(bl57);
	
	dungeon::misc bl58;
	bl58.m_sprite.set_position(640 + 1280, 655);
	map.add_block(bl58);
	
	dungeon::misc bl59;
	bl59.m_sprite.set_position(640 + 1280, 615);
	map.add_block(bl59);
	
	dungeon::misc bl60;
	bl60.m_sprite.set_position(575 + 1280, 615);
	map.add_block(bl60);
	
	dungeon::misc bl61;
	bl61.m_sprite.set_position(575 + 1280, 590);
	map.add_block(bl61);
	
	dungeon::misc bl62;
	bl62.m_sprite.set_position(605 + 1280, 590);
	map.add_block(bl62);
	
	dungeon::misc bl63;
	bl63.m_sprite.set_position(640 + 1280, 590);
	map.add_block(bl63);
	
	dungeon::misc bl64;
	bl64.m_sprite.set_position(640 + 1280, 500);
	map.add_block(bl64);
	
	dungeon::misc bl65;
	bl65.m_sprite.set_position(605 + 1280, 500);
	map.add_block(bl65);
	
	dungeon::misc bl66;
	bl66.m_sprite.set_position(575 + 1280, 500);
	map.add_block(bl66);
	
	dungeon::misc bl67;
	bl67.m_sprite.set_position(640 + 1280, 495);
	map.add_block(bl67);
	
	dungeon::misc bl68;
	bl68.m_sprite.set_position(575 + 1280, 495);
	map.add_block(bl68);
	
	dungeon::misc bl69;
	bl69.m_sprite.set_position(640 + 1280, 465);
	map.add_block(bl69);
	
	dungeon::misc bl70;
	bl70.m_sprite.set_position(545 + 1280, 465);
	map.add_block(bl70);
	
	dungeon::misc bl71;
	bl71.m_sprite.set_position(575 + 1280, 465);
	map.add_block(bl71);
	
	dungeon::misc bl72;
	bl72.m_sprite.set_position(640 + 1280, 425);
	map.add_block(bl72);
	
	dungeon::misc bl73;
	bl73.m_sprite.set_position(505 + 1280, 425);
	map.add_block(bl73);
	
	dungeon::misc bl74;
	bl74.m_sprite.set_position(575 + 1280,822);
	map.add_block(bl74);
	
	dungeon::misc bl75;
	bl75.m_sprite.set_position(575 + 1280,847);
	map.add_block(bl75);
	
	dungeon::misc bl76;
	bl76.m_sprite.set_position(640 + 1280,847);
	map.add_block(bl76);
	
	dungeon::misc bl77;
	bl77.m_sprite.set_position(640 + 1280,822);
	map.add_block(bl77);
	
	dungeon::misc bl78;
	bl78.m_sprite.set_position(466 + 1280,391);
	map.add_block(bl78);
	
	dungeon::misc bl79;
	bl79.m_sprite.set_position(432 + 1280, 391);
	map.add_block(bl79);
	
	dungeon::misc bl80;
	bl80.m_sprite.set_position(404 + 1280, 391);
	map.add_block(bl80);
	
	dungeon::misc bl81;
	bl81.m_sprite.set_position(378 + 1280, 379);
	map.add_block(bl81);
	
	dungeon::misc bl82;
	bl82.m_sprite.set_position(378 + 1280, 364);
	map.add_block(bl82);
	
	dungeon::misc bl83;
	bl83.m_sprite.set_position(380 + 1280, 337);
	map.add_block(bl83);
	
	dungeon::misc bl84;
	bl84.m_sprite.set_position(679 + 1280, 360);
	map.add_block(bl84);
	
	dungeon::misc bl85;
	bl85.m_sprite.set_position(670 + 1280, 340);
	map.add_block(bl85);
	
	dungeon::misc bl86;
	bl86.m_sprite.set_position(659 + 1280, 392);
	map.add_block(bl86);
	
	dungeon::misc bl87;
	bl87.m_sprite.set_position(575 + 1280, 530);
	map.add_block(bl87);
	
	dungeon::misc bl88;
	bl88.m_sprite.set_position(575 + 1280, 557);
	map.add_block(bl88);
	
	dungeon::misc bl89;
	bl89.m_sprite.set_position(640 + 1280, 557);
	map.add_block(bl89);
	
	dungeon::misc bl90;
	bl90.m_sprite.set_position(640 + 1280, 530);
	map.add_block(bl90);
	// Textures management
	auto const & t0_0 = thoth::textures().load_extern("t0_0", "../media/texture/Kazzador/npc/VXHeroA/hero_0-0.png");
	auto const & t0_1 = thoth::textures().load_extern("t0_1", "../media/texture/Kazzador/npc/VXHeroA/hero_0-1.png");
	auto const & t0_2 = thoth::textures().load_extern("t0_2", "../media/texture/Kazzador/npc/VXHeroA/hero_0-2.png");
	auto const & t1_0 = thoth::textures().load_extern("t1_0", "../media/texture/Kazzador/npc/VXHeroA/hero_1-0.png");
	auto const & t1_1 = thoth::textures().load_extern("t1_1", "../media/texture/Kazzador/npc/VXHeroA/hero_1-1.png");
	auto const & t1_2 = thoth::textures().load_extern("t1_2", "../media/texture/Kazzador/npc/VXHeroA/hero_1-2.png");
	auto const & t2_0 = thoth::textures().load_extern("t2_0", "../media/texture/Kazzador/npc/VXHeroA/hero_2-0.png");
	auto const & t2_1 = thoth::textures().load_extern("t2_1", "../media/texture/Kazzador/npc/VXHeroA/hero_2-1.png");
	auto const & t2_2 = thoth::textures().load_extern("t2_2", "../media/texture/Kazzador/npc/VXHeroA/hero_2-2.png");
	auto const & t3_0 = thoth::textures().load_extern("t3_0", "../media/texture/Kazzador/npc/VXHeroA/hero_3-0.png");
	auto const & t3_1 = thoth::textures().load_extern("t3_1", "../media/texture/Kazzador/npc/VXHeroA/hero_3-1.png");
	auto const & t3_2 = thoth::textures().load_extern("t3_2", "../media/texture/Kazzador/npc/VXHeroA/hero_3-2.png");
	
	dungeon::camera cam(setting, map);
	dungeon::hero p1(t0_0, "Palindor", 30.f, 11.f, 12.f, 13.f);
	
	auto const & h0_0 = thoth::textures().load_extern("h0_0", "../media/texture/Kazzador/orc/orc_0-0.png");
	auto const & h0_1 = thoth::textures().load_extern("h0_1", "../media/texture/Kazzador/orc/orc_0-1.png");
	auto const & h0_2 = thoth::textures().load_extern("h0_2", "../media/texture/Kazzador/orc/orc_0-2.png");
	auto const & h1_0 = thoth::textures().load_extern("h1_0", "../media/texture/Kazzador/orc/orc_1-0.png");
	auto const & h1_1 = thoth::textures().load_extern("h1_1", "../media/texture/Kazzador/orc/orc_1-1.png");
	auto const & h1_2 = thoth::textures().load_extern("h1_2", "../media/texture/Kazzador/orc/orc_1-2.png");
	auto const & h2_0 = thoth::textures().load_extern("h2_0", "../media/texture/Kazzador/orc/orc_2-0.png");
	auto const & h2_1 = thoth::textures().load_extern("h2_1", "../media/texture/Kazzador/orc/orc_2-1.png");
	auto const & h2_2 = thoth::textures().load_extern("h2_2", "../media/texture/Kazzador/orc/orc_2-2.png");
	auto const & h3_0 = thoth::textures().load_extern("h3_0", "../media/texture/Kazzador/orc/orc_3-0.png");
	auto const & h3_1 = thoth::textures().load_extern("h3_1", "../media/texture/Kazzador/orc/orc_3-1.png");
	auto const & h3_2 = thoth::textures().load_extern("h3_2", "../media/texture/Kazzador/orc/orc_3-2.png");
	
	bool stop = false;
	bool ko = false;
	dungeon::npc orc1(h0_0, "Orc1");
	orc1.m_sprite.set_position(1000, 552);
	
	orc1.m_sprite.add("left", 0.2f, { h1_1, h1_0, h1_2, h1_0 });
	orc1.m_sprite.add("right", 0.2f, { h2_1,  h2_0, h2_2, h2_0 });
	orc1.m_sprite.add("up", 0.2f, { h3_1, h3_0, h3_2, h3_0 });
	orc1.m_sprite.add("down", 0.2f ,{ h0_1, h0_0, h0_2, h0_0 });
	orc1.m_sprite.add("left_static", 0.2f, { h1_0});
	orc1.m_sprite.add("right_static", 0.2f, { h2_0});
	orc1.m_sprite.add("up_static", 0.2f, { h3_0});
	orc1.m_sprite.add("down_static", 0.2f , { h0_0});
	
	map.add_hostil(orc1);
	
	// Animated sprite
	p1.get_hero().m_sprite.add("left", 0.2f, { t1_1, t1_0, t1_2, t1_0 });
	p1.get_hero().m_sprite.add("right", 0.2f, { t2_1,  t2_0, t2_2, t2_0 });
	p1.get_hero().m_sprite.add("up", 0.2f, { t3_1, t3_0, t3_2, t3_0 });
	p1.get_hero().m_sprite.add("down", 0.2f ,{ t0_1, t0_0, t0_2, t0_0 });
	p1.get_hero().m_sprite.add("left_static", 0.2f, { t1_0});
	p1.get_hero().m_sprite.add("right_static", 0.2f, { t2_0});
	p1.get_hero().m_sprite.add("up_static", 0.2f, { t3_0});
	p1.get_hero().m_sprite.add("down_static", 0.2f , { t0_0});
	p1.get_hero().m_sprite.set_position(30.f, 552.f);	
	
	sf::Clock clock;
	
	auto const & map1_room = thoth::textures().load_extern("map1_room", "../media/texture/Dungeon/map/room1-p0.png");
	dungeon::map map1(map1_room, 448.f, 225.f);
	load_room_test(map1);
	
	dungeon::camera cam1(setting, map1);
	
	std::vector<dungeon::map> maps;
	maps.push_back(map);
	maps.push_back(map1);
	
	std::vector<dungeon::camera> cams;
	cams.push_back(cam);
	cams.push_back(cam1);
	
	bool hide_blocks = true;
	
	std::size_t index = 0;
	
	// Start
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			
			// Key released
			else if (event.type == sf::Event::KeyReleased)
			{
				if (event.key.code == sf::Keyboard::Escape)
				{
					window.close();
				}
				else if (event.key.code == sf::Keyboard::C)
				{
					std::cout << "X: " << p1.get_hero().m_sprite.position().x << " Y: "<< p1.get_hero().m_sprite.position().y << std::endl;
				}
				else if (event.key.code == sf::Keyboard::O)
				{
					hide_blocks = true;
				}
				else if (event.key.code == sf::Keyboard::P)
				{
					hide_blocks = false;
				}
			}
		}
		
		if(thoth::collision(p1.get_hero().m_sprite, maps[0].teleporter().at(2).m_sprite))
		{
			index = 1;
			p1.get_hero().m_sprite.set_position(225, 170);	
		}
		
		if(thoth::collision(p1.get_hero().m_sprite, maps[1].teleporter().at(0).m_sprite))
		{
			index = 0;
			p1.get_hero().m_sprite.set_position(2448, 530);	
		}
		if (index == 0)
		{
			p1.get_hero().m_sprite.set_position
			(
				std::min(std::max(p1.get_hero().m_sprite.position().x,0.f),3840.f),
			std::max(std::min(p1.get_hero().m_sprite.position().y,896.f), 315.f )
			);
		}
		else if (index == 1)
		{
			p1.get_hero().m_sprite.set_position
			(
				std::min(std::max(p1.get_hero().m_sprite.position().x,0.f),448.f),
			 std::max(std::min(p1.get_hero().m_sprite.position().y,225.f), 0.f )
			);
		}
		// Time
		float const elapsed = clock.restart().asSeconds();
		
		float const moving = 150 * elapsed;
		
		if(hide_blocks)
		{
			maps[0].teleporter().at(0).m_anim.animate("a");
			maps[0].teleporter().at(0).m_anim.update(elapsed);
			maps[0].teleporter().at(1).m_anim.animate("a");
			maps[0].teleporter().at(1).m_anim.update(elapsed);
		}
		
		if(thoth::collision(p1.get_hero().m_sprite, tp1.m_anim))
		{
			teleport.play();
			p1.get_hero().m_sprite.set_position(2050, 552);
		}
		if(thoth::collision(p1.get_hero().m_sprite, tp2.m_anim))
		{
			teleport.play();
			p1.get_hero().m_sprite.set_position(1700, 552);
		}
		
		// Keyboard
		auto p1_previous_position = p1.get_hero().m_sprite.position();
		p1.move(moving);
		
		// 
		for (std::size_t i = 0; i < maps[index].hostils().size(); i++)
		{
			if(maps[index].hostils().at(i).m_sprite.position().x <= 1400 && !stop)
			{
				maps[index].hostils().at(i).m_sprite.animate("right");
				maps[index].hostils().at(i).m_sprite.move(moving, 0);
			}
			else if(maps[index].hostils().at(i).m_sprite.position().x >= 1400)
			{
				stop = true;
			}
			
			if(maps[index].hostils().at(i).m_sprite.position().x >= 1000 && stop)
			{
				maps[index].hostils().at(i).m_sprite.animate("left");
				maps[index].hostils().at(i).m_sprite.move(-moving, 0);
			}
			else if (maps[index].hostils().at(i).m_sprite.position().x <= 1000)
			{
				stop = false;
			}
		}
	
		// Player view
		cams[index].view(p1);
		maps[index].border(window, p1);
		
		// Clear the screen
		window.clear(sf::Color::Black);
		
		maps[index].draw(window);
		
		for(std::size_t i = 0; i < maps[index].hostils().size(); i++)
		{
			maps[index].hostils().at(i).m_sprite.update(elapsed);
		}
		
		p1.get_hero().m_sprite.update(elapsed);
		
		
		/// Combat
		for (std::size_t i = 0; i < maps[index].hostils().size(); i++)
		{
			if (thoth::collision(p1.get_hero().m_sprite, maps[index].hostils().at(i).m_sprite) && !ko)
			{
				if(dungeon::combat::fight(window, setting, p1.get_hero(), maps[index].hostils().at(i)))
				{
					ko = true; 
				}
				else
				{
					p1.get_hero().m_sprite.set_position(30.f, 552.f);
					p1.get_hero().set_mana(100);
					p1.get_hero().set_life(100);
				}
			}
		}
		
		/// Collisions
		for (int i = 0; i < maps[index].miscs().size(); i++)
		{
			if(thoth::collision(p1.get_hero().m_sprite, maps[index].miscs().at(i).m_sprite))
			{
				p1.get_hero().m_sprite.set_position(p1_previous_position);
			}
		}
		
		/// Collisions
		for (int i = 0; i < maps[index].blocks().size(); i++)
		{
			if(thoth::collision(p1.get_hero().m_sprite, maps[index].blocks().at(i).m_sprite))
			{
				p1.get_hero().m_sprite.set_position(p1_previous_position);
			}
		}
		
		/// Draw Statics
		for (auto const & sprite : maps[index].statics())
		{
			window << sprite;
		}
		
		/// Draw Miscs
		for (auto const & sprite : maps[index].miscs())
		{
			window << sprite;
		}
		
		if (!hide_blocks){
			/// Draw blocks
			for (auto const & sprite : maps[index].blocks())
			{
				window << sprite;
			}
		}
		
		for (auto const & sprite : maps[index].teleporter())
		{
			window << sprite;
		}
		window << p1;
		
		for (std::size_t i = 0; i < maps[index].hostils().size(); i++)
		{
			if (!ko)
			{
				window << maps[index].hostils().at(i);
			}
		}
		
		window.setView(cams[index].getView());
		
		/// Draw Cursor
		sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
		sf::Vector2f worldPos = window.mapPixelToCoords(pixelPos);
		setting.cursor().set_position(worldPos.x, worldPos.y);
		window << setting.cursor();
		
		// Display	
		window.display();
		
		
	}
	
	return 0;
}

