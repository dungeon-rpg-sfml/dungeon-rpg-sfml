// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2014 Kevin Vinchon, kevin.vinchon@u-psud.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <SFML/Graphics.hpp>

#include <thoth/textures.hpp>
#include <thoth/sprite.hpp>
#include <thoth/collision.hpp>

#include <dungeon/misc.hpp>
#include <dungeon/map.hpp>
#include <dungeon/hero.hpp>
#include <dungeon/camera.hpp>
#include <dungeon/placard.hpp>
#include <dungeon/setting.hpp>

int main() 
{
	dungeon::setting setting;
	
	sf::RenderWindow window(sf::VideoMode(1024,768), "test");
	setting.set_size_window(sf::Vector2u(1024, 768));
	
	// Background Map
	auto const & map_room = thoth::textures().load_extern("room", "../media/texture/Dungeon/map/room1-p0.png");
	dungeon::map map(map_room, 448.f, 225.f);
	
	load_room_test(map);
	
	// Textures management
	auto const & t0_0 = thoth::textures().load_extern("t0_0", "../media/texture/Kazzador/npc/VXHeroA/hero_0-0.png");
	auto const & t0_1 = thoth::textures().load_extern("t0_1", "../media/texture/Kazzador/npc/VXHeroA/hero_0-1.png");
	auto const & t0_2 = thoth::textures().load_extern("t0_2", "../media/texture/Kazzador/npc/VXHeroA/hero_0-2.png");
	auto const & t1_0 = thoth::textures().load_extern("t1_0", "../media/texture/Kazzador/npc/VXHeroA/hero_1-0.png");
	auto const & t1_1 = thoth::textures().load_extern("t1_1", "../media/texture/Kazzador/npc/VXHeroA/hero_1-1.png");
	auto const & t1_2 = thoth::textures().load_extern("t1_2", "../media/texture/Kazzador/npc/VXHeroA/hero_1-2.png");
	auto const & t2_0 = thoth::textures().load_extern("t2_0", "../media/texture/Kazzador/npc/VXHeroA/hero_2-0.png");
	auto const & t2_1 = thoth::textures().load_extern("t2_1", "../media/texture/Kazzador/npc/VXHeroA/hero_2-1.png");
	auto const & t2_2 = thoth::textures().load_extern("t2_2", "../media/texture/Kazzador/npc/VXHeroA/hero_2-2.png");
	auto const & t3_0 = thoth::textures().load_extern("t3_0", "../media/texture/Kazzador/npc/VXHeroA/hero_3-0.png");
	auto const & t3_1 = thoth::textures().load_extern("t3_1", "../media/texture/Kazzador/npc/VXHeroA/hero_3-1.png");
	auto const & t3_2 = thoth::textures().load_extern("t3_2", "../media/texture/Kazzador/npc/VXHeroA/hero_3-2.png");
	
	dungeon::camera cam(setting, map);
	dungeon::hero p1(t0_0, "Palindor", 30.f, 11.f, 12.f, 13.f);
	
	// Animated sprite
	p1.get_hero().m_sprite.add("left", 0.2f, { t1_1, t1_0, t1_2, t1_0 });
	p1.get_hero().m_sprite.add("right", 0.2f, { t2_1,  t2_0, t2_2, t2_0 });
	p1.get_hero().m_sprite.add("up", 0.2f, { t3_1, t3_0, t3_2, t3_0 });
	p1.get_hero().m_sprite.add("down", 0.2f ,{ t0_1, t0_0, t0_2, t0_0 });
	p1.get_hero().m_sprite.add("left_static", 0.2f, { t1_0});
	p1.get_hero().m_sprite.add("right_static", 0.2f, { t2_0});
	p1.get_hero().m_sprite.add("up_static", 0.2f, { t3_0});
	p1.get_hero().m_sprite.add("down_static", 0.2f , { t0_0});
	p1.get_hero().m_sprite.set_position(225, 170);	
	
	sf::Clock clock;
	
	// Start
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			
			// Key released
			else if (event.type == sf::Event::KeyReleased)
			{
				if (event.key.code == sf::Keyboard::Escape)
				{
					window.close();
				}
				else if (event.key.code == sf::Keyboard::C)
				{
					std::cout << "X: " << p1.get_hero().m_sprite.position().x << " Y: "<< p1.get_hero().m_sprite.position().y << std::endl;
				}
			}
		}
		
		// Time
		float const elapsed = clock.restart().asSeconds();
		
		float const moving = 150 * elapsed;
		
		// Keyboard
		auto p1_previous_position = p1.get_hero().m_sprite.position();
		p1.move(moving);
		
		// Player viewm_sprite.
		cam.view(p1);
		map.border(window, p1);
		
		// Clear the screen
		window.clear(sf::Color::Black);
		
		map.draw(window);
		
		p1.get_hero().m_sprite.update(elapsed);
		
		/// Collisions
		for (int i = 0; i < map.miscs().size(); i++)
		{
			if(thoth::collision(p1.get_hero().m_sprite, map.miscs().at(i).m_sprite))
			{
				p1.get_hero().m_sprite.set_position(p1_previous_position);
			}
		}
		
		/// Collisions
		for (int i = 0; i < map.blocks().size(); i++)
		{
			if(thoth::collision(p1.get_hero().m_sprite, map.blocks().at(i).m_sprite))
			{
				p1.get_hero().m_sprite.set_position(p1_previous_position);
			}
		}
		
		/// Draw Miscs
		for (auto const & sprite : map.miscs())
		{
			window << sprite;
		}
		
		/// Draw blocks
		for (auto const & sprite : map.blocks())
		{
			window << sprite;
		}
		
		window << p1;
		
		window.setView(cam.getView());
		
		// Display	
		window.display();
		
		
	}
	
	return 0;
}
