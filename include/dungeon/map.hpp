 // Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
 
 // Licensed under the Apache License, Version 2.0 (the "License");
 // you may not use this file except in compliance with the License.
 // You may obtain a copy of the License at
 // 
 // http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software
 // distributed under the License is distributed on an "AS IS" BASIS,
 // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
 #ifndef DUNGEON_RPG_SFML_MAP_HPP
 #define DUNGEON_RPG_SFML_MAP_HPP
 
 #include <thoth/sprite.hpp>
 #include <thoth/textures.hpp>
 #include "hero.hpp"
 #include "misc.hpp"
 #include "teleporter.hpp"
 #include "placard.hpp"
 
 namespace dungeon
 {
	 
	 /**
	  * @brief Map
	  *
	  * @code
	  *   #include <thoth/sprite.hpp>
	  *   @endcode
	  */

	 class map
	 {
	 private:
		 /// Background
		 thoth::sprite m_sprite;
		 float m_height;
		 float m_width;
		 
		 /// Villagers
		 std::vector<dungeon::npc> m_passifs;
		 
		 /// Misc
		 std::vector<dungeon::misc> m_miscs;
		 
		 std::vector<dungeon::misc> m_statics;
		 
		 /// Blocks
		 std::vector<dungeon::misc> m_blocks;
 		 
		 /// Teleporters
		 std::vector<dungeon::teleporter> m_teleporters;
		 
		 /// Placard
		 std::vector<dungeon::placard> m_placards;
		 
	 public:
		 
		 /// Monsters
		 std::vector<dungeon::npc> m_hostils;
		 
		 /// @brief Constructor
		 map() = default;
		 
		 /// @brief Constructor
		 /// @param[in] texture Default texture (thoth::texture::default_texture() by default)
		 map(thoth::texture const & default_texture,float const height,float const width):
		 m_sprite(default_texture),
		 m_height(height),
		 m_width(width)
		 { }
		 
		 
		 void border(sf::RenderWindow & window, dungeon::hero & p1)
		 {
			 p1.get_hero().m_sprite.set_position
			 (
			  std::min(std::max(p1.get_hero().m_sprite.position().x,0.f),m_height),
			  std::max(std::min(p1.get_hero().m_sprite.position().y,m_width), 0.f )
			 );
		 }
		 
		 void draw(sf::RenderWindow & window)
		 {
			window << m_sprite;
		 }
		 
		 float & height()
		 {
			 return m_height;
		 }
		 
		 float & width()
		 {
			return m_width; 
		 }
		 
		 void add_block(dungeon::misc const & m)
		 {
			m_blocks.push_back(m); 
		 }
		 
		 std::vector<dungeon::misc> & blocks()
		 {
			return m_blocks; 
		 }
		 
		 void add(dungeon::misc const & m)
		 {
			m_miscs.push_back(m);
		 }
		 
		 std::vector<dungeon::misc> & miscs()
		 {
			return m_miscs;
		 }
		 
		 void add_static(dungeon::misc const & s)
		 {
			m_statics.push_back(s); 
		 }
		 
		 std::vector<dungeon::misc> & statics()
		 {
			return m_statics; 
		 }
		 
		 void add_hostil(dungeon::npc const & a)
		 {
			 m_hostils.push_back(a);
		 }
		 
		 std::vector<dungeon::npc> & hostils()
		 {
			 return m_hostils;
		 }
		 
		 void add_passif(dungeon::npc const & a)
		 {
			 m_passifs.push_back(a);
		 }
		 
		 std::vector<dungeon::npc> & passifs()
		 {
			 return m_passifs;
		 }
		 
		 void add(dungeon::placard const & a)
		 {
			 m_placards.push_back(a);
		 }
		 
		 std::vector<dungeon::placard> & placard()
		 {
			 return m_placards;
		 }
		 
		 void add_teleporter(dungeon::teleporter const & a)
		 {
			 m_teleporters.push_back(a);
		 }
		 
		 std::vector<dungeon::teleporter> &  teleporter()
		 {
			 return m_teleporters;
		 }
	 };
	 
	 inline void load_village(dungeon::map & map)
	 {
		 /// Trees
		 auto const & misc_tree_1 = thoth::textures().load_extern("tree_1", "../media/texture/Dungeon/misc/tree_1.png");
		 
		 
		 dungeon::misc t5(misc_tree_1);
		 t5.m_sprite.set_position(45, 750);
		 map.add(t5);
		 
		 dungeon::misc t4(misc_tree_1);
		 t4.m_sprite.set_position(85,780);
		 map.add(t4);
		 
		 dungeon::misc t2(misc_tree_1);
		 t2.m_sprite.set_position(10,790);
		 map.add(t2);
		 
		 dungeon::misc t1(misc_tree_1);
		 t1.m_sprite.set_position(15,840);
		 map.add(t1);
		 
		 dungeon::misc t3(misc_tree_1);
		 t3.m_sprite.set_position(70,865);
		 map.add(t3);
		 
		 
		 dungeon::misc t6(misc_tree_1);
		 t6.m_sprite.set_position(0,315);
		 map.add(t6);
		 dungeon::misc t7(misc_tree_1);
		 t7.m_sprite.set_position(270,315);
		 map.add(t7);
		 dungeon::misc t8(misc_tree_1);
		 t8.m_sprite.set_position(290,380);
		 map.add(t8);
		 dungeon::misc t9(misc_tree_1);
		 t9.m_sprite.set_position(340,310);
		 map.add(t9);
		 dungeon::misc t10(misc_tree_1);
		 t10.m_sprite.set_position(1095,320);
		 map.add(t10);
		 dungeon::misc t11(misc_tree_1);
		 t11.m_sprite.set_position(1065,340);
		 map.add(t11);
		 dungeon::misc t12(misc_tree_1);
		 t12.m_sprite.set_position(1125,340);
		 map.add(t12);
		 dungeon::misc t13(misc_tree_1);
		 t13.m_sprite.set_position(1220,320);
		 map.add(t13);
		 dungeon::misc t14(misc_tree_1);
		 t14.m_sprite.set_position(1190,340);
		 map.add(t14);
		 dungeon::misc t15(misc_tree_1);
		 t15.m_sprite.set_position(1255,340);
		 map.add(t15);
		 dungeon::misc t16(misc_tree_1);
		 t16.m_sprite.set_position(1220,400);
		 map.add(t16);
		 dungeon::misc t17(misc_tree_1);
		 t17.m_sprite.set_position(1190,410);
		 map.add(t17);
		 dungeon::misc t18(misc_tree_1);
		 t18.m_sprite.set_position(1255,410);
		 map.add(t18);
		 
		 auto const & misc_tree_2 = thoth::textures().load_extern("tree_2", "../media/texture/Dungeon/misc/tree_2.png");
		 dungeon::misc tt1(misc_tree_2);
		 tt1.m_sprite.set_position(890,250);
		 map.add(tt1);
		 dungeon::misc tt2(misc_tree_2);
		 tt2.m_sprite.set_position(980,250);
		 map.add(tt2);
		 dungeon::misc tt3(misc_tree_2);
		 tt3.m_sprite.set_position(1090,350);
		 map.add(tt3);
		 dungeon::misc tt4(misc_tree_2);
		 tt4.m_sprite.set_position(1210,350);
		 map.add(tt4);
		 dungeon::misc tt5(misc_tree_2);
		 tt5.m_sprite.set_position(1210,410);
		 map.add(tt5);
		 
		 /// Dead Trees
		 auto const & misc_dead_tree_1 = thoth::textures().load_extern("dead_tree_1", "../media/texture/Dungeon/misc/dead_tree_1.png");
		 dungeon::misc dt1(misc_dead_tree_1);
		 dt1.m_sprite.set_position(45,430);
		 map.add(dt1);
		 dungeon::misc dt2(misc_dead_tree_1);
		 dt2.m_sprite.set_position(720,320);
		 map.add(dt2);
		 dungeon::misc dt3(misc_tree_1);
		 dt3.m_sprite.set_position(485,720);
		 map.add(dt3);
		 
		 /// Bridges
		 auto const & misc_bridge_1 = thoth::textures().load_extern("bridge_1", "../media/texture/Dungeon/misc/bridge_1.png");
		 dungeon::misc b1(misc_bridge_1);
		 b1.m_sprite.set_position(605,550);
		 map.add_static(b1);
		 
		 dungeon::misc b2(misc_bridge_1);
		 b2.m_sprite.set_position(605,840);
		 map.add_static(b2);
		 
		 /// misc
		 auto const & misc_wood_1 = thoth::textures().load_extern("wood_1", "../media/texture/Dungeon/misc/wood_1.png");
		 dungeon::misc w1(misc_wood_1);
		 w1.m_sprite.set_position(234,424);
		 map.add(w1);
		 dungeon::misc w2(misc_wood_1);
		 w2.m_sprite.set_position(800,480);
		 map.add(w2);
		 
		 auto const & misc_well_1 = thoth::textures().load_extern("well_1", "../media/texture/Dungeon/misc/well_1.png");
		 dungeon::misc wl1(misc_well_1);
		 wl1.m_sprite.set_position(360,620);
		 map.add(wl1);
		 
		 auto const & misc_rock_1 = thoth::textures().load_extern("rock_1", "../media/texture/Dungeon/misc/rock_1.png");
		 dungeon::misc rk1(misc_rock_1);
		 rk1.m_sprite.set_position(400,410);
		 map.add(rk1);
		 dungeon::misc rk2(misc_rock_1);
		 rk2.m_sprite.set_position(440,405);
		 map.add(rk2);
		 dungeon::misc rk3(misc_rock_1);
		 rk3.m_sprite.set_position(460,415);
		 map.add(rk3);
		 
		 /// Houses
		 auto const & house_house_1 = thoth::textures().load_extern("house_1", "../media/texture/Dungeon/house/house_1.png");
		 dungeon::misc h1(house_house_1);
		 h1.m_sprite.set_position(127,330);
		 map.add(h1);
		 
		 auto const & house_house_2 = thoth::textures().load_extern("house_2", "../media/texture/Dungeon/house/house_2.png");
		 dungeon::misc h2(house_house_2);
		 h2.m_sprite.set_position(182,680);
		 map.add(h2);
		 
		 auto const & house_house_3 = thoth::textures().load_extern("house_3", "../media/texture/Dungeon/house/house_3.png");
		 dungeon::misc h3(house_house_3);
		 h3.m_sprite.set_position(910,370);
		 map.add(h3);
		 
		 auto const & house_house_4 = thoth::textures().load_extern("house_4", "../media/texture/Dungeon/house/house_4.png");
		 dungeon::misc h4(house_house_4);
		 h4.m_sprite.set_position(990,700);
		 map.add(h4);
		 
		 /// Barrels
		 auto const & misc_barrel_1 = thoth::textures().load_extern("barrel_1", "../media/texture/Dungeon/misc/barrel_1.png");
		 dungeon::misc br1(misc_barrel_1);
		 br1.m_sprite.set_position(205,762);
		 map.add(br1);
		 dungeon::misc br2(misc_barrel_1);
		 br2.m_sprite.set_position(850,430);
		 map.add(br2);
		 
		 /// Fences
		 auto const & fences_fence_1 = thoth::textures().load_extern("fence_1", "../media/texture/Dungeon/fences/fence_1.png");
		 dungeon::misc f1(fences_fence_1);
		 f1.m_sprite.set_position(1055,460);
		 map.add(f1);
		 
		 /// Hay
		 auto const & misc_hay_1 = thoth::textures().load_extern("hay_1", "../media/texture/Dungeon/misc/hay_1.png");
		 dungeon::misc hay1(misc_hay_1);
		 hay1.m_sprite.set_position(1050,460);
		 map.add(hay1);
		 
		 auto const & misc_hay_2 = thoth::textures().load_extern("hay_2", "../media/texture/Dungeon/misc/hay_2.png");
		 dungeon::misc hay2(misc_hay_2);
		 hay2.m_sprite.set_position(1100,455);
		 map.add(hay2);
		 
		 /// Bushes
		 auto const & grass_bush_1 = thoth::textures().load_extern("bush_1", "../media/texture/Dungeon/grass/bush_1.png");
		 dungeon::misc bh1(grass_bush_1);
		 bh1.m_sprite.set_position(874,755);
		 map.add(bh1);
		 dungeon::misc bh2(grass_bush_1);
		 bh2.m_sprite.set_position(899,755);
		 map.add(bh2);
		 dungeon::misc bh3(grass_bush_1);
		 bh3.m_sprite.set_position(924,755);
		 map.add(bh3);
		 dungeon::misc bh4(grass_bush_1);
		 bh4.m_sprite.set_position(1030,755);
		 map.add(bh4);
		 dungeon::misc bh5(grass_bush_1);
		 bh5.m_sprite.set_position(1055,755);
		 map.add(bh5);
		 dungeon::misc bh6(grass_bush_1);
		 bh6.m_sprite.set_position(1080,755);
		 map.add(bh6);
		 dungeon::misc bh7(grass_bush_1);
		 bh7.m_sprite.set_position(1105,755);
		 map.add(bh7);
		 dungeon::misc bh8(grass_bush_1);
		 bh8.m_sprite.set_position(874,770);
		 map.add(bh8);
		 dungeon::misc bh9(grass_bush_1);
		 bh9.m_sprite.set_position(899,770);
		 map.add(bh9);
		 dungeon::misc bh10(grass_bush_1);
		 bh10.m_sprite.set_position(924,770);
		 map.add(bh10);
		 dungeon::misc bh11(grass_bush_1);
		 bh11.m_sprite.set_position(1030,770);
		 map.add(bh11);
		 dungeon::misc bh12(grass_bush_1);
		 bh12.m_sprite.set_position(1055,770);
		 map.add(bh12);
		 dungeon::misc bh13(grass_bush_1);
		 bh13.m_sprite.set_position(1080,770);
		 map.add(bh13);
		 dungeon::misc bh14(grass_bush_1);
		 bh14.m_sprite.set_position(1105,770);
		 map.add(bh14);
		 
		 /// Placard
		 auto const & placard_placard_1 = thoth::textures().load_extern("placard_1", "../media/texture/Dungeon/placard/placard_1.png");
		 dungeon::placard pa1(placard_placard_1);
		 pa1.m_sprite.set_position(130,510);
		 map.add(pa1);
		 
		 auto const & placard_placard_2 = thoth::textures().load_extern("placard_2", "../media/texture/Dungeon/placard/placard_2.png");
		 dungeon::placard pa2(placard_placard_2);
		 pa2.m_sprite.set_position(870,510);
		 map.add(pa2);
		 
		 /// Plants
		 auto const & grass_plant_1 = thoth::textures().load_extern("plant_1", "../media/texture/Dungeon/grass/plant_1.png");
		 dungeon::misc pl1(grass_plant_1);
		 pl1.m_sprite.set_position(255,490);
		 map.add(pl1);
		 dungeon::misc pl2(grass_plant_1);
		 pl2.m_sprite.set_position(280,490);
		 map.add(pl2);
		 dungeon::misc pl3(grass_plant_1);
		 pl3.m_sprite.set_position(305,490);
		 map.add(pl3);
		 dungeon::misc pl4(grass_plant_1);
		 pl4.m_sprite.set_position(330,490);
		 map.add(pl4);
		 dungeon::misc pl5(grass_plant_1);
		 pl5.m_sprite.set_position(355,490);
		 map.add(pl5);
		 dungeon::misc pl6(grass_plant_1);
		 pl6.m_sprite.set_position(380,490);
		 map.add(pl6);
		 dungeon::misc pl7(grass_plant_1);
		 pl7.m_sprite.set_position(405,490);
		 map.add(pl7);
		 dungeon::misc pl8(grass_plant_1);
		 pl8.m_sprite.set_position(430,490);
		 map.add(pl8);
		 dungeon::misc pl9(grass_plant_1);
		 pl9.m_sprite.set_position(455,490);
		 map.add(pl9);
		 dungeon::misc pl10(grass_plant_1);
		 pl10.m_sprite.set_position(480,490);
		 map.add(pl10);
		 
		 
		 /// Blocks
		 dungeon::misc bl1;
		 bl1.m_sprite.set_position(640,880);
		 map.add_block(bl1);
		 
		 dungeon::misc bl2;
		 bl2.m_sprite.set_position(575,880);
		 map.add_block(bl2);
		 
		 dungeon::misc bl3;
		 bl3.m_sprite.set_position(605,880);
		 map.add_block(bl3);
		 
		 dungeon::misc bl4;
		 bl4.m_sprite.set_position(575,755);
		 map.add_block(bl4);
		 
		 dungeon::misc bl5;
		 bl5.m_sprite.set_position(640,755);
		 map.add_block(bl5);
		 
		 dungeon::misc bl6;
		 bl6.m_sprite.set_position(575, 790);
		 map.add_block(bl6);
		 
		 dungeon::misc bl7;
		 bl7.m_sprite.set_position(605, 790);
		 map.add_block(bl7);
		 
		 dungeon::misc bl8;
		 bl8.m_sprite.set_position(640, 790);
		 map.add_block(bl8);
		 
		 dungeon::misc bl9;
		 bl9.m_sprite.set_position(575, 790);
		 map.add_block(bl9);
		 
		 dungeon::misc bl10;
		 bl10.m_sprite.set_position(575, 725);
		 map.add_block(bl10);
		 
		 dungeon::misc bl11;
		 bl11.m_sprite.set_position(640, 725);
		 map.add_block(bl11);
		 
		 dungeon::misc bl12;
		 bl12.m_sprite.set_position(640, 690);
		 map.add_block(bl12);
		 
		 dungeon::misc bl13;
		 bl13.m_sprite.set_position(575, 690);
		 map.add_block(bl13);
		 
		 dungeon::misc bl14;
		 bl14.m_sprite.set_position(575, 655);
		 map.add_block(bl14);
		 
		 dungeon::misc bl15;
		 bl15.m_sprite.set_position(640, 655);
		 map.add_block(bl15);
		 
		 dungeon::misc bl16;
		 bl16.m_sprite.set_position(640, 615);
		 map.add_block(bl16);
		 
		 dungeon::misc bl17;
		 bl17.m_sprite.set_position(575, 615);
		 map.add_block(bl17);
		 
		 dungeon::misc bl18;
		 bl18.m_sprite.set_position(575, 590);
		 map.add_block(bl18);
		 
		 dungeon::misc bl19;
		 bl19.m_sprite.set_position(605, 590);
		 map.add_block(bl19);
		 
		 dungeon::misc bl20;
		 bl20.m_sprite.set_position(640, 590);
		 map.add_block(bl20);
		 
		 dungeon::misc bl21;
		 bl21.m_sprite.set_position(640, 500);
		 map.add_block(bl21);
		 
		 dungeon::misc bl22;
		 bl22.m_sprite.set_position(605, 500);
		 map.add_block(bl22);
		 
		 dungeon::misc bl23;
		 bl23.m_sprite.set_position(575, 500);
		 map.add_block(bl23);
		 
		 dungeon::misc bl24;
		 bl24.m_sprite.set_position(640, 495);
		 map.add_block(bl24);
		 
		 dungeon::misc bl25;
		 bl25.m_sprite.set_position(575, 495);
		 map.add_block(bl25);
		 
		 dungeon::misc bl26;
		 bl26.m_sprite.set_position(640, 465);
		 map.add_block(bl26);
		 
		 dungeon::misc bl27;
		 bl27.m_sprite.set_position(545, 465);
		 map.add_block(bl27);
		 
		 dungeon::misc bl28;
		 bl28.m_sprite.set_position(575, 465);
		 map.add_block(bl28);
		 
		 dungeon::misc bl29;
		 bl29.m_sprite.set_position(640, 425);
		 map.add_block(bl29);
		 
		 dungeon::misc bl30;
		 bl30.m_sprite.set_position(505, 425);
		 map.add_block(bl30);
		 
		 /// Teleporter
		 dungeon::teleporter tp1;
		 tp1.m_sprite.set_position(980, 790);
		 map.add_teleporter(tp1);
		 
		 dungeon::teleporter tp2;
		 tp2.m_sprite.set_position(1275, 552);
		 map.add_teleporter(tp2);
	 }
	 
	 void load_room_test(dungeon::map & map)
	 {
		 // Room
		 auto const & room_room1_p1 = thoth::textures().load_extern("room1_p1", "../media/texture/Dungeon/room/room1-p1.png");
		 dungeon::misc p1_1(room_room1_p1);
		 p1_1.m_sprite.set_position(223,10);
		 map.add(p1_1);
		 
		 auto const & room_room1_p2 = thoth::textures().load_extern("room1_p2", "../media/texture/Dungeon/room/room1-p2.png");
		 dungeon::misc p2_1(room_room1_p2);
		 p2_1.m_sprite.set_position(340,130);
		 map.add(p2_1);
		 
		 auto const & room_room1_p3 = thoth::textures().load_extern("room1_p3", "../media/texture/Dungeon/room/room1-p3.png");
		 dungeon::misc p3_1(room_room1_p3);
		 p3_1.m_sprite.set_position(70,45);
		 map.add(p3_1);
		 
		 auto const & room_room1_p4 = thoth::textures().load_extern("room1_p4", "../media/texture/Dungeon/room/room1-p4.png");
		 dungeon::misc p4_1(room_room1_p4);
		 p4_1.m_sprite.set_position(190,35);
		 map.add(p4_1);
		 
		 auto const & room_room1_p5 = thoth::textures().load_extern("room1_p5", "../media/texture/Dungeon/room/room1-p5.png");
		 dungeon::misc p5_1(room_room1_p5);
		 p5_1.m_sprite.set_position(260,25);
		 map.add(p5_1);
		 
		 dungeon::misc p5_2(room_room1_p5);
		 p5_2.m_sprite.set_position(320,25);
		 map.add(p5_2);
		 
		 auto const & room_room1_p6 = thoth::textures().load_extern("room1_p6", "../media/texture/Dungeon/room/room1-p6.png");
		 dungeon::misc p6_1(room_room1_p6);
		 p6_1.m_sprite.set_position(400,40);
		 map.add(p6_1);
		 
		 auto const & room_room1_p8 = thoth::textures().load_extern("room1_p8", "../media/texture/Dungeon/room/room1-p8.png");
		 dungeon::misc p8_1(room_room1_p8);
		 p8_1.m_sprite.set_position(20,120);
		 map.add(p8_1);
		 
		 /// Blocks
		 dungeon::misc bl1;
		 bl1.m_sprite.set_position(135,210);
		 map.add_block(bl1);
		 
		 dungeon::misc bl2;
		 bl2.m_sprite.set_position(102,210);
		 map.add_block(bl2);
		 
		 dungeon::misc bl3;
		 bl3.m_sprite.set_position(70,210);
		 map.add_block(bl3);
		 
		 dungeon::misc bl4;
		 bl4.m_sprite.set_position(37,210);
		 map.add_block(bl4);
		 
		 dungeon::misc bl5;
		 bl5.m_sprite.set_position(10,210);
		 map.add_block(bl5);
		 
		 dungeon::misc bl6;
		 bl6.m_sprite.set_position(430,210);
		 map.add_block(bl6);
		 
		 dungeon::misc bl7;
		 bl7.m_sprite.set_position(400,210);
		 map.add_block(bl7);
		 
		 dungeon::misc bl8;
		 bl8.m_sprite.set_position(375,210);
		 map.add_block(bl8);
		 
		 dungeon::misc bl9;
		 bl9.m_sprite.set_position(350,210);
		 map.add_block(bl9);
		 
		 dungeon::misc bl10;
		 bl10.m_sprite.set_position(310,210);
		 map.add_block(bl10);
		 
		 /// Teleporter
		 dungeon::teleporter tp1;
		 tp1.m_sprite.set_position(222, 206);
		 map.add_teleporter(tp1);
		 
	 }
	 
	 inline void load_boss_room(dungeon::map & map)
	 {
		 //Boss textures
		 auto const & boss_boss_p1 = thoth::textures().load_extern("boss_p1", "../media/texture/Dungeon/boss/boss-p1.png");
		 dungeon::misc bp1(boss_boss_p1);
		 bp1.m_sprite.set_position(272,35);
		 map.add(bp1);
		 
		 auto const & boss_boss_p2 = thoth::textures().load_extern("boss_p2", "../media/texture/Dungeon/boss/boss-p2.png");
		 dungeon::misc bp2_1(boss_boss_p2);
		 bp2_1.m_sprite.set_position(15,60);
		 map.add(bp2_1);
		 
		 dungeon::misc bp2_2(boss_boss_p2);
		 bp2_2.m_sprite.set_position(528,60);
		 map.add(bp2_2);
		 
		 dungeon::misc bp2_3(boss_boss_p2);
		 bp2_3.m_sprite.set_position(15,290);
		 map.add(bp2_3);
		 
		 dungeon::misc bp2_4(boss_boss_p2);
		 bp2_4.m_sprite.set_position(528,290);
		 map.add(bp2_4);
		 
		 auto const & boss_boss_p3 = thoth::textures().load_extern("boss_p3", "../media/texture/Dungeon/boss/boss-p3.png");
		 dungeon::misc bp3(boss_boss_p3);
		 bp3.m_sprite.set_position(275,60);
		 map.add(bp3);
		 
		 auto const & boss_boss_p4 = thoth::textures().load_extern("boss_p4", "../media/texture/Dungeon/boss/boss-p4.png");
		 dungeon::misc bp4(boss_boss_p4);
		 bp4.m_sprite.set_position(272,353);
		 map.add_static(bp4);
		 
		 auto const & boss_boss_p5 = thoth::textures().load_extern("boss_p5", "../media/texture/Dungeon/boss/boss-p5.png");
		 dungeon::misc bp5_1(boss_boss_p5);
		 bp5_1.m_sprite.set_position(210,450);
		 map.add(bp5_1);
		 
		 dungeon::misc bp5_2(boss_boss_p5);
		 bp5_2.m_sprite.set_position(335,450);
		 map.add(bp5_2);
		 
		 /// Blocks
		 dungeon::misc bl1;
		 bl1.m_sprite.set_position(405,500);
		 map.add_block(bl1);
		 
		 dungeon::misc bl2;
		 bl2.m_sprite.set_position(405,465);
		 map.add_block(bl2);
		 
		 dungeon::misc bl3;
		 bl3.m_sprite.set_position(405,425);
		 map.add_block(bl3);
		 
		 dungeon::misc bl4;
		 bl4.m_sprite.set_position(405,395);
		 map.add_block(bl4);
		 
		 dungeon::misc bl5;
		 bl5.m_sprite.set_position(405,365);
		 map.add_block(bl5);
		 
		 dungeon::misc bl6;
		 bl6.m_sprite.set_position(405, 340);
		 map.add_block(bl6);
		 
		 dungeon::misc bl7;
		 bl7.m_sprite.set_position(430, 340);
		 map.add_block(bl7);
		 
		 dungeon::misc bl8;
		 bl8.m_sprite.set_position(460, 340);
		 map.add_block(bl8);
		 
		 dungeon::misc bl9;
		 bl9.m_sprite.set_position(495, 340);
		 map.add_block(bl9);
		 
		 dungeon::misc bl10;
		 bl10.m_sprite.set_position(525, 340);
		 map.add_block(bl10);
		 
		 dungeon::misc bl11;
		 bl11.m_sprite.set_position(140, 460);
		 map.add_block(bl11);
		 
		 dungeon::misc bl12;
		 bl12.m_sprite.set_position(140, 495);
		 map.add_block(bl12);
		 
		 dungeon::misc bl13;
		 bl13.m_sprite.set_position(140,425);
		 map.add_block(bl13);
		 
		 dungeon::misc bl14;
		 bl14.m_sprite.set_position(140,395);
		 map.add_block(bl14);
		 
		 dungeon::misc bl15;
		 bl15.m_sprite.set_position(140,365);
		 map.add_block(bl15);
		 
		 dungeon::misc bl16;
		 bl16.m_sprite.set_position(140, 340);
		 map.add_block(bl16);
		 
		 dungeon::misc bl17;
		 bl17.m_sprite.set_position(120, 340);
		 map.add_block(bl17);
		 
		 dungeon::misc bl18;
		 bl18.m_sprite.set_position(87, 340);
		 map.add_block(bl18);
		 
		 dungeon::misc bl19;
		 bl19.m_sprite.set_position(55, 340);
		 map.add_block(bl19);
		 
		 dungeon::misc bl20;
		 bl20.m_sprite.set_position(20, 340);
		 map.add_block(bl20);
		 
		 /// Teleporter
		 dungeon::teleporter tp1;
		 tp1.m_sprite.set_position(275, 505);
		 map.add_teleporter(tp1);
	 }
	 
	 inline void load_forest_1(dungeon::map & map)
	 {
		 //Boss's house
		 auto const & house1 = thoth::textures().load_extern("house1", "../media/texture/Dungeon/house/house_1.png");
		 dungeon::misc h1(house1);
		 h1.m_sprite.set_position(245,430);
		 map.add(h1);
		 
		 // Dead tree
		 auto const & misc_dead_tree_1 = thoth::textures().load_extern("dead_tree_1", "../media/texture/Dungeon/misc/dead_tree_1.png");
		 dungeon::misc dt1(misc_dead_tree_1);
		 dt1.m_sprite.set_position(155, 480);
		 map.add(dt1);
		 
		 //Trees
		 auto const & misc_tree_1 = thoth::textures().load_extern("tree_1", "../media/texture/Dungeon/misc/tree_1.png");
		 dungeon::misc t1(misc_tree_1);
		 t1.m_sprite.set_position(40,55);
		 map.add(t1);
		 
		 dungeon::misc t2(misc_tree_1);
		 t2.m_sprite.set_position(140,55);
		 map.add(t2);
		 
		 dungeon::misc t3(misc_tree_1);
		 t3.m_sprite.set_position(245,55);
		 map.add(t3);
		 
		 dungeon::misc t4(misc_tree_1);
		 t4.m_sprite.set_position(350,55);
		 map.add(t4);
		 
		 dungeon::misc t5(misc_tree_1);
		 t5.m_sprite.set_position(455,55);
		 map.add(t5);
		 
		 dungeon::misc t6(misc_tree_1);
		 t6.m_sprite.set_position(570,55);
		 map.add(t6);
		 
		 dungeon::misc t7(misc_tree_1);
		 t7.m_sprite.set_position(690,55);
		 
		 map.add(t7);
		 
		 dungeon::misc t8(misc_tree_1);
		 t8.m_sprite.set_position(1180,55);
		 map.add(t8);
		 
		 dungeon::misc t9(misc_tree_1);
		 t9.m_sprite.set_position(1240,160);
		 map.add(t9);
		 
		 dungeon::misc t10(misc_tree_1);
		 t10.m_sprite.set_position(740,160);
		 map.add(t10);
		 
		 dungeon::misc t11(misc_tree_1);
		 t11.m_sprite.set_position(625,160);
		 map.add(t11);
		 
		 dungeon::misc t12(misc_tree_1);
		 t12.m_sprite.set_position(510,160);
		 map.add(t12);
		 
		 dungeon::misc t13(misc_tree_1);
		 t13.m_sprite.set_position(415,160);
		 map.add(t13);
		 
		 dungeon::misc t14(misc_tree_1);
		 t14.m_sprite.set_position(300,160);
		 map.add(t14);
		 
		 dungeon::misc t15(misc_tree_1);
		 t15.m_sprite.set_position(200,160);
		 map.add(t15);
		 
		 dungeon::misc t16(misc_tree_1);
		 t16.m_sprite.set_position(80,160);
		 map.add(t16);
		 
		 dungeon::misc t17(misc_tree_1);
		 t17.m_sprite.set_position(25,270);
		 map.add(t17);
		 
		 dungeon::misc t18(misc_tree_1);
		 t18.m_sprite.set_position(145,270);
		 map.add(t18);
		 
		 dungeon::misc t19(misc_tree_1);
		 t19.m_sprite.set_position(255,270);
		 map.add(t19);
		 
		 dungeon::misc t20(misc_tree_1);
		 t20.m_sprite.set_position(350,270);
		 map.add(t20);
		 
		 dungeon::misc t21(misc_tree_1);
		 t21.m_sprite.set_position(1240,290);
		 map.add(t21);
		 
		 dungeon::misc t22(misc_tree_1);
		 t22.m_sprite.set_position(740,300);
		 map.add(t22);
		 
		 dungeon::misc t23(misc_tree_1);
		 t23.m_sprite.set_position(650,360);
		 map.add(t23);
		 
		 dungeon::misc t24(misc_tree_1);
		 t24.m_sprite.set_position(440,385);
		 map.add(t24);
		 
		 dungeon::misc t25(misc_tree_1);
		 t25.m_sprite.set_position(550,410);
		 map.add(t25);
		 
		 dungeon::misc t26(misc_tree_1);
		 t26.m_sprite.set_position(1215,425);
		 map.add(t26);
		 
		 dungeon::misc t27(misc_tree_1);
		 t27.m_sprite.set_position(35,440);
		 map.add(t27);
		 
		 dungeon::misc t28(misc_tree_1);
		 t28.m_sprite.set_position(1130,495);
		 map.add(t28);
		 
		 dungeon::misc t29(misc_tree_1);
		 t29.m_sprite.set_position(1030,550);
		 map.add(t29);
		 
		 dungeon::misc t30(misc_tree_1);
		 t30.m_sprite.set_position(110,575);
		 map.add(t30);
		 
		 dungeon::misc t31(misc_tree_1);
		 t31.m_sprite.set_position(940,630);
		 map.add(t31);
		 
		 dungeon::misc t32(misc_tree_1);
		 t32.m_sprite.set_position(840,700);
		 map.add(t32);
		 
		 dungeon::misc t33(misc_tree_1);
		 t33.m_sprite.set_position(200,700);
		 map.add(t33);
		 
		 dungeon::misc t34(misc_tree_1);
		 t34.m_sprite.set_position(325,715);
		 map.add(t34);
		 
		 dungeon::misc t35(misc_tree_1);
		 t35.m_sprite.set_position(430,715);
		 map.add(t35);
		 
		 dungeon::misc t36(misc_tree_1);
		 t36.m_sprite.set_position(525,715);
		 map.add(t36);
		 
		 dungeon::misc t37(misc_tree_1);
		 t37.m_sprite.set_position(630,715);
		 map.add(t37);
		 
		 dungeon::misc t38(misc_tree_1);
		 t38.m_sprite.set_position(740,715);
		 map.add(t38);
		 
// 		 // Dead Trees
// 		 auto const & misc_dead_tree_1 = thoth::textures().load_extern("dead_tree_1", "../media/texture/Dungeon/misc/dead_tree_1.png");
// 		 dungeon::misc dt1(misc_dead_tree_1);
// 		 dt1.m_sprite.set_position(560,280);
// 		 map.add(dt1);
		 
		 dungeon::teleporter tp1;
		 tp1.m_sprite.set_position(903, 20);
		 map.add_teleporter(tp1);
		 
		 dungeon::teleporter tp2;
		 tp2.m_sprite.set_position(290, 500);
		 map.add_teleporter(tp2);
	 }
 }
 
 #endif
 