// Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_MENU_HPP
#define DUNGEON_RPG_SFML_MENU_HPP

#include <SFML/Audio.hpp>
#include <TGUI/TGUI.hpp>

#include "menu/main_menu.hpp"
#include "game.hpp"

namespace dungeon
{
	
	/**
	 * @brief Provides menu functions
	 *
	 * @code
	   #include <TGUI/TGUI.hpp>
	   #include <SFML/Audio.hpp>
	   #include "menu/main_menu.hpp"
	   @endcode
	 */
	namespace menu
	{
		// For Doxygen only
	}
}

#endif
 
