// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_HERO_HPP
#define DUNGEON_RPG_SFML_HERO_HPP

#include <hnc/serialization.hpp>
#include <hnc/geometry.hpp>

#include "dungeon/npc.hpp"
#include "setting.hpp"

namespace dungeon
{
	/**
	 * @brief Hero
	 * 
	 * @code
	   #include <dungeon/npc.hpp>
	   @endcode
	 * 
	 */
	
	class hero 
	{
	private:
	
		/// NPC
		dungeon::npc m_hero;
		
		sf::Keyboard::Key up = sf::Keyboard::Up;
		sf::Keyboard::Key down = sf::Keyboard::Down;
		sf::Keyboard::Key left = sf::Keyboard::Left;
		sf::Keyboard::Key right = sf::Keyboard::Right;
		
	public:
		
		/// @brief Constructor
		/// @param[in] default_texture Default texture of the main character's sprite
		/// @param[in] strength        Strength value of the main character
		/// @param[in] adress          Adress value of the main character
		/// @param[in] intelligence    Intelligence value of the main character
		/// @param[in] luck            Luck value of the main character
		hero(thoth::texture const & default_texture, std::string const & name, float strength, float adress, float intelligence, float luck) : m_hero(default_texture, name)
		{ 
			m_hero.set_strength(strength);
			m_hero.set_adress(adress);
			m_hero.set_intelligence(intelligence);
			m_hero.set_luck(luck);
			m_hero.set_damage(m_hero.damage() + m_hero.strength()/10);
		}
		
		/// @brief Move the player's sprite according to the key pressed
		/// @param[in] moving
		void move (float const moving)
		{
			if (sf::Keyboard::isKeyPressed(left))
			{
				m_hero.m_sprite.animate("left");
				m_hero.m_sprite.move(-moving, 0);
			}
			else if (sf::Keyboard::isKeyPressed(right))
			{
				m_hero.m_sprite.animate("right");
				m_hero.m_sprite.move(moving, 0);
			}
			else if (sf::Keyboard::isKeyPressed(up))
			{
				m_hero.m_sprite.animate("up");
				m_hero.m_sprite.move(0, -moving);
			}
			else if (sf::Keyboard::isKeyPressed(down))
			{
				m_hero.m_sprite.animate("down");
				m_hero.m_sprite.move(0, moving);
			}
			else
			{
				if (m_hero.m_sprite.animation() == "left")
				{
					m_hero.m_sprite.animate("left_static");
				}
				else if (m_hero.m_sprite.animation() == "right")
				{
					m_hero.m_sprite.animate("right_static");
				}
				else if (m_hero.m_sprite.animation() == "up")
				{
					m_hero.m_sprite.animate("up_static");
				}
				else if (m_hero.m_sprite.animation() == "down")
				{
					m_hero.m_sprite.animate("down_static");
				}
				else if (m_hero.m_sprite.animation() == "")
				{
					m_hero.m_sprite.reset();
				}
			}
		}
		
		void set_setting(dungeon::setting const & setting)
		{
			left = setting.keys[0];
			right = setting.keys[1];
			up = setting.keys[2];
			down = setting.keys[3];
		}
		
		/// @brief Return npc hero's object
		/// @return m_hero
		dungeon::npc const & get_hero() const
		{
			return m_hero;
		}
		
		/// @brief Return npc hero's object
		/// @return m_hero
		dungeon::npc & get_hero()
		{
			return m_hero;
		}
	};

	
	sf::RenderWindow & operator<<(sf::RenderWindow & window, dungeon::hero const & hero)
	{
		window << hero.get_hero();
		return  window;
	}
}
#endif
