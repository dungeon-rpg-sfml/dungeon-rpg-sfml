// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

#ifndef DUNGEON_RPG_SFML_PLACARD_HPP
#define DUNGEON_RPG_SFML_PLACARD_HPP

#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>

#include <thoth/sprite_centered.hpp>
#include <thoth/textures.hpp>

namespace dungeon
{
	/**
	 * @brief Placard
	 * 
	 * @code
	   #include <thoth/sprite_centered.hpp>
	   #include <thoth/texture.hpp>
	   @endcode
	 * 
	 */
	class placard 
	{
	private:
		/// String in Text
		std::string m_description;
		
		/// Size of Letters
		int m_police;
		
		/// Type of Letters' police
		std::string m_police_type;
		
		/// Text
		sf::Text m_text;
		
		/// Font
		sf::Font m_font;
		
		/// Boolean use to detect if the text is draw
		bool m_is_open = false;
	public:
		/// Placard's Sprite
		thoth::sprite_centered m_sprite;
		
		/// @brief Constructor
		/// @param[in] texture        Texture of the placard
		/// @param[in] description    Message in the Text
		/// @param[in] police_type    Type of letters in the Text
		/// @param[in] police         Size of Letters
		placard(thoth::texture const & texture, std::string description = "Hello world", std::string police_type = "../media/police/arthur.ttf", int police = 50) : m_sprite(texture)
		{
			m_description = description;
			m_police = police;
			m_police_type = police_type;
			m_font.loadFromFile(m_police_type);
		} 
		
		/// @brief Draw the message text in the window
		/// @param[in] window
		void post(sf::RenderWindow &window)
		{
				m_is_open = true;
				m_text.setFont(m_font);
				m_text.setString(m_description);
				m_text.setCharacterSize(m_police);
				m_text.setColor(sf::Color::Blue);
				m_text.setPosition(m_sprite.position().x - (m_description.size() * m_police)/(m_description.size()), m_sprite.position().y +m_police);
				window.draw(m_text);
		}
		
		
		
		/// @brief Set m_is_open false
		void close()
		{
			m_is_open = false;
		}
		
		/// @brief Set m_is_open true
		void open() 
		{
			m_is_open = true;
		}
		
		/// @brief Return the message is drawn
		/// @return true is the message is draw, false otherwise
		bool is_open() const
		{
			return m_is_open;
		}
	};
	
}

#endif
