// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_SETTING_HPP
#define DUNGEON_RPG_SFML_SETTING_HPP

#include <hnc/serialization.hpp>

#include <SFML/Graphics.hpp>

#include <thoth/sprite.hpp>
#include <thoth/textures.hpp>

namespace dungeon
{
	/**
	 * @brief Setting
	 * 
	 * @code
	 * #include <SFML/Graphics.hpp>
	 * @endcode
	 * 
	 */
	
	class setting 
	{
	private:
		
		/// Fullscreen
		bool m_fullscreen;
		
		/// Mouse Cursor
		thoth::sprite m_cursor;
		
		/// Window's Size
		sf::Vector2u m_size;
		
	public:
		
		/// Keyboard
		std::vector<sf::Keyboard::Key> keys;
		
		sf::Keyboard::Key up = sf::Keyboard::Up;
		sf::Keyboard::Key down = sf::Keyboard::Down;
		sf::Keyboard::Key left = sf::Keyboard::Left;
		sf::Keyboard::Key right = sf::Keyboard::Right;
		
		/// Music's volume
		std::size_t m_music_volume;
		
		/// Sound's volume
		std::size_t m_sound_volume;
		
        /// @brief Constructor
		setting() = default;
		
		setting(thoth::texture const & default_texture) : 
		m_cursor(default_texture),
		m_music_volume(100),
		m_sound_volume(100),
		m_size(800, 600),
		m_fullscreen(false)
		{
			keys.push_back(left);
			keys.push_back(right);
			keys.push_back(up);
			keys.push_back(down);
		}
		
		std::string get_id_key(int const & i) const
		{
			if(keys[i] == sf::Keyboard::A)
			{
				return "A";
			}
			else if (keys[i] == sf::Keyboard::Z)
			{
				return "Z";
			}
			else if (keys[i] == sf::Keyboard::E)
			{
				return "E";
			}
			else if (keys[i] == sf::Keyboard::R)
			{
				return "R";
			}
			else if (keys[i] == sf::Keyboard::T)
			{
				return "T";
			}
			else if (keys[i] == sf::Keyboard::Y)
			{
				return "Y";
			}
			else if (keys[i] == sf::Keyboard::U)
			{
				return "U";
			}
			else if (keys[i] == sf::Keyboard::I)
			{
				return "I";
			}
			else if (keys[i] == sf::Keyboard::O)
			{
				return "O";
			}
			else if (keys[i] == sf::Keyboard::P)
			{
				return "P";
			}
			else if (keys[i] == sf::Keyboard::Q)
			{
				return "Q";
			}
			else if (keys[i] == sf::Keyboard::S)
			{
				return "S";
			}
			else if (keys[i] == sf::Keyboard::D)
			{
				return "D";
			}
			else if (keys[i] == sf::Keyboard::F)
			{
				return "F";
			}
			else if (keys[i] == sf::Keyboard::G)
			{
				return "G";
			}
			else if (keys[i] == sf::Keyboard::H)
			{
				return "H";
			}
			else if (keys[i] == sf::Keyboard::J)
			{
				return "J";
			}
			else if (keys[i] == sf::Keyboard::K)
			{
				return "K";
			}
			else if (keys[i] == sf::Keyboard::L)
			{
				return "L";
			}
			else if (keys[i] == sf::Keyboard::M)
			{
				return "M";
			}
			else if (keys[i] == sf::Keyboard::W)
			{
				return "W";
			}
			else if (keys[i] == sf::Keyboard::X)
			{
				return "X";
			}
			else if (keys[i] == sf::Keyboard::C)
			{
				return "C";
			}
			else if (keys[i] == sf::Keyboard::V)
			{
				return "V";
			}
			else if (keys[i] == sf::Keyboard::B)
			{
				return "B";
			}
			else if (keys[i] == sf::Keyboard::N)
			{
				return "N";
			}
			else if (keys[i] == sf::Keyboard::Left)
			{
				return "Left";
			}
			else if (keys[i] == sf::Keyboard::Right)
			{
				return "Right";
			}
			else if (keys[i] == sf::Keyboard::Up)
			{
				return "Up";
			}
			else if (keys[i] == sf::Keyboard::Down)
			{
				return "Down";
			}
			else
			{
				return "Unknown";
			}
		}
		
		bool identical_key() const
		{
			for (std::size_t i = 0; i < keys.size(); i++)
			{
				for (std::size_t j = i+1; j < keys.size(); j++)
				{
					if(keys[i] == keys[j] && keys[i] != sf::Keyboard::Unknown )
					{
						return true;
					}
				}
			}
			return false;
		}
		void set_size_window(sf::Vector2u const & size)
		{
			m_size = size;
		}
		
		sf::Vector2u size_window() const
		{
			return m_size;
		}
		
		void set_fullscreen() 
		{
			m_fullscreen = true;
		}
		
		void set_no_fullscreen() 
		{
			m_fullscreen = false;
		}
		
		bool fullscreen() const 
		{
			return m_fullscreen;
		}
		
		void set_music_volume(std::size_t const & volume)
		{
			m_music_volume = volume;
		}
		
		void set_sound_volume(std::size_t const & volume)
		{
			m_sound_volume = volume;
		}
		
		std::size_t & music_volume() 
		{
			return m_music_volume;
		}
		
		std::size_t & sound_volume() 
		{
			return m_sound_volume;
		}
		
		thoth::sprite & cursor()
		{
			return m_cursor;
		}
		
	};
	
}
#endif 

