 // Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
 
 // Licensed under the Apache License, Version 2.0 (the "License");
 // you may not use this file except in compliance with the License.
 // You may obtain a copy of the License at
 // 
 // http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software
 // distributed under the License is distributed on an "AS IS" BASIS,
 // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
 #ifndef DUNGEON_RPG_SFML_LOAD_BUTTON_HPP
 #define DUNGEON_RPG_SFML_LOAD_BUTTON_HPP
 
 #include <TGUI/TGUI.hpp>
 #include <SFML/Audio.hpp>
 #include "../setting.hpp"
 
 namespace dungeon
 {
	 namespace menu
	 {
		 /**
		  * @brief Main Menu 
		  * 
		  * @code
		  * #include <TGUI/TGUI.hpp>
		  * #include <SFML/Audio.hpp>
		  * #include "../setting.hpp"
		  * @endcode
		  * 
		  * @param[in] window Window of the game
		  */
		 inline void load_button( sf::RenderWindow & window, dungeon::setting & setting, sf::Sound & sound )
		 {  
			 tgui::Gui gui(window);
			 
			 // Load the font (you should check the return value to make sure that it is loaded)
			 gui.setGlobalFont("../media/police/Roman_SD.ttf");
			 
			 // Create background Paper picture
			 tgui::Picture::Ptr pic_paper(gui);
			 pic_paper->load("../media/texture/Lamoot/UI/Background/paper_background.png");
			 
			 // Create background Dragon picture
			 tgui::Picture::Ptr pic_dragon(gui);
			 pic_dragon->load("../media/texture/Dungeon/dev/menu.png");
			 
			 // Create Save 1 button
			 tgui::Button::Ptr btn_save_1(gui);
			 btn_save_1->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_save_1->setSize(260, 60);
			 btn_save_1->setText("Save 1");
			 btn_save_1->setTextSize(25);
			 btn_save_1->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_save_1->setCallbackId(1);
			 
			 // Create Save 2 button
			 tgui::Button::Ptr btn_save_2(gui);
			 btn_save_2->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_save_2->setSize(260, 60);
			 btn_save_2->setText("Save 2");
			 btn_save_2->setTextSize(25);
			 btn_save_2->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_save_2->setCallbackId(2);
			 
			 // Create Save 3 button
			 tgui::Button::Ptr btn_save_3(gui);
			 btn_save_3->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_save_3->setSize(260, 60);
			 btn_save_3->setText("Save 3");
			 btn_save_3->setTextSize(25);
			 btn_save_3->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_save_3->setCallbackId(3);
			 
			 // Create Back button
			 tgui::Button::Ptr btn_back(gui);
			 btn_back->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_back->setSize(260, 60);
			 btn_back->setText("Back");
			 btn_back->setTextSize(25);
			 btn_back->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_back->setCallbackId(4);
			 
			 
			 // Main loop
			 while (window.isOpen())
			 {
				 sf::Event event;
				 while (window.pollEvent(event))
				 {
					 if (event.type == sf::Event::Closed)
						 window.close();
					 
					 // Pass the event to all the widgets
					 gui.handleEvent(event);
				 }
				 
				 // The callback loop
				 tgui::Callback callback;
				 while (gui.pollCallback(callback))
				 {
					 // Make sure tha callback comes from the button
					 switch(callback.id)
					 {
						 case 1: sound.play(); std::cout<<"SAVE 1"<<std::endl;break;
						 case 2: sound.play(); std::cout<<"SAVE 2"<<std::endl;break;
						 case 3: sound.play(); std::cout<<"SAVE 3"<<std::endl;break;
						 case 4: sound.play(); return; break;
					 }
				 }
				 // Update widget's position
				 pic_paper->setSize(setting.size_window().x,setting.size_window().y);
				 pic_dragon->setSize(setting.size_window().x,setting.size_window().y);
				 btn_save_1->setPosition((setting.size_window().x/2)-130, setting.size_window().y/6);
				 btn_save_2->setPosition((setting.size_window().x/2)-130, setting.size_window().y/3);
				 btn_save_3->setPosition((setting.size_window().x/2)-130, setting.size_window().y/2);
				 btn_back->setPosition((setting.size_window().x/2)-130, setting.size_window().y/1.5);
				 
				 window.clear();
				 
				 // Draw all created widgets
				 gui.draw();
				 setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
				 window << setting.cursor();
				 window.display();
				 
			 }
		 }
	 }
 }
 
 #endif
 
