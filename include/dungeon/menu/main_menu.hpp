// Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_MENU_MAIN_MENU_HPP
#define DUNGEON_RPG_SFML_MENU_MAIN_MENU_HPP

#include <TGUI/TGUI.hpp>
#include <SFML/Audio.hpp>

#include <thoth/sprite.hpp>
#include <thoth/textures.hpp>

#include "load_button.hpp"
#include "option_button.hpp"
#include "../setting.hpp"
#include "../game.hpp"

namespace dungeon
{
	namespace menu
	{
		/**
		* @brief Main Menu 
		* 
		* @code
		  #include <TGUI/TGUI.hpp>
		  #include <SFML/Audio.hpp>
		  #include "load_button.hpp"
		  #include "option_button.hpp"
		  #include "../setting.hpp"
		  #include "../game.hpp"
		  @endcode
		* 
		* @param[in] window Window of the game
		*/
		void main_menu (dungeon::game & game)
		{
			
			sf::Music music;
			music.openFromFile("../media/music/Gobusto/rpg_music/yesterbreeze.ogg");
			music.play();
			music.setLoop(true);
			
			sf::SoundBuffer buffer_click;
			buffer_click.loadFromFile("../media/sound/LFA/mouse_click/mouseclick.ogg");
			
			sf::Sound sound;
			sound.setBuffer(buffer_click);
			
			tgui::Gui gui(game.window());
			
			// Load the font (you should check the return value to make sure that it is loaded)
			gui.setGlobalFont("../media/police/Roman_SD.ttf");
			
			// Create background Paper picture
			tgui::Picture::Ptr pic_paper(gui);
			pic_paper->load("../media/texture/Lamoot/UI/Background/paper_background.png");
			
			// Create background Dragon picture
			tgui::Picture::Ptr pic_dragon(gui);
			pic_dragon->load("../media/texture/Dungeon/dev/menu.png");
			
			// Create New Game button
			tgui::Button::Ptr btn_new_game(gui);
			btn_new_game->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			btn_new_game->setSize(260, 60);
			btn_new_game->setText("New game");
			btn_new_game->setTextSize(25);
			btn_new_game->bindCallback(tgui::Button::LeftMouseClicked);
			btn_new_game->setCallbackId(1);
			
			// Create load button
			tgui::Button::Ptr btn_load(gui);
			btn_load->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			btn_load->setSize(260, 60);
			btn_load->setText("Load");
			btn_load->setTextSize(25);
			btn_load->bindCallback(tgui::Button::LeftMouseClicked);
			btn_load->setCallbackId(2);
			
			// Create Option button
			tgui::Button::Ptr btn_option(gui);
			btn_option->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			btn_option->setSize(260, 60);
			btn_option->setText("Option");
			btn_option->setTextSize(25);
			btn_option->bindCallback(tgui::Button::LeftMouseClicked);
			btn_option->setCallbackId(3);
			
			// Create Exit button
			tgui::Button::Ptr btn_exit(gui);
			btn_exit->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			btn_exit->setSize(260, 60);
			btn_exit->setText("Exit");
			btn_exit->setTextSize(25);
			btn_exit->bindCallback(tgui::Button::LeftMouseClicked);
			btn_exit->setCallbackId(4);
			
			// Main loop
			while (game.window().isOpen())
			{
				sf::Event event;
				while (game.window().pollEvent(event))
				{
					if (event.type == sf::Event::Closed)
						game.window().close();
					else if (event.type == sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
						game.window().close();
					// Pass the event to all the widgets
					gui.handleEvent(event);
				}
				
				// The callback loop
				tgui::Callback callback;
				while (gui.pollCallback(callback))
				{
					// Make sure tha callback comes from the button
					switch(callback.id)
					{
						case 1: sound.play(); music.stop(); game.run();break;
						case 2: sound.play(); dungeon::menu::load_button(game.window(), game.setting(), sound);break;
						case 3: sound.play(); dungeon::menu::option_button(game.window(), game.setting(), music, sound);break;
						case 4: sound.play(); return; break;
					}
				}
				
			// Update widget's position
			pic_paper->setSize(game.setting().size_window().x,game.setting().size_window().y);
			pic_dragon->setSize(game.setting().size_window().x,game.setting().size_window().y);
			btn_new_game->setPosition((game.setting().size_window().x/2)-130, game.setting().size_window().y/6);
			btn_load->setPosition((game.setting().size_window().x/2)-130, game.setting().size_window().y/3);
			btn_option->setPosition((game.setting().size_window().x/2)-130, game.setting().size_window().y/2);
			btn_exit->setPosition((game.setting().size_window().x/2)-130, game.setting().size_window().y/1.5);
			
			// Music and sound volume
			music.setVolume(game.setting().music_volume());
			sound.setVolume(game.setting().sound_volume());
			
// 			std::cout << "music " << setting.music_volume() << " sound " << setting.sound_volume() << std::endl;
			
			game.window().clear();
			// Draw all created widgets
			gui.draw();
			
			game.setting().cursor().set_position(sf::Mouse::getPosition(game.window()).x, sf::Mouse::getPosition(game.window()).y);
			game.window() << game.setting().cursor();
			game.window().display();
			
			}
		}
	}
	
}

#endif
