// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
 
#ifndef DUNGEON_RPG_SFML_AUDIO_BUTTON_HPP
#define DUNGEON_RPG_SFML_AUDIO_BUTTON_HPP
 
#include <TGUI/TGUI.hpp>
#include <SFML/Audio.hpp>
#include "../setting.hpp"
 
namespace dungeon
{
	namespace menu
	{
		/**
		 * @brief Audio  Button
		 * 
		 * @code
		 * #include <TGUI/TGUI.hpp>
		 * #include <SFML/Audio.hpp>
		 * #include "../setting.hpp"
		 * @endcode
		 * 
		 * @param[in] window Window of the game
		 */
		inline void audio_button(sf::RenderWindow & window, dungeon::setting & setting, sf::Music & music, sf::Sound & sound)
		{  
			tgui::Gui gui(window);
			
			// Load the font (you should check the return value to make sure that it is loaded)
			gui.setGlobalFont("../media/police/Roman_SD.ttf");
			
			// Create background Paper picture
			tgui::Picture::Ptr pic_paper(gui);
			pic_paper->load("../media/texture/Lamoot/UI/Background/paper_background.png");
			
			// Create background Dragon picture
			tgui::Picture::Ptr pic_dragon(gui);
			pic_dragon->load("../media/texture/Dungeon/dev/menu.png");
			
			// Create Music Label
			tgui::Label::Ptr lbl_music(gui);
			lbl_music->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			lbl_music->setText("Music");
			lbl_music->setTextColor(sf::Color(255, 255, 255));
			lbl_music->setTextSize(24);
			
			// Create Music Slider
			tgui::Slider::Ptr slider_music(gui);
			slider_music->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			slider_music->setVerticalScroll(false);
			slider_music->setMinimum(0);
			slider_music->setMaximum(100);
			slider_music->setValue(setting.music_volume());
			
			// Create Sound Label
			tgui::Label::Ptr lbl_sound(gui);
			lbl_sound->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			lbl_sound->setText("Sound");
			lbl_sound->setTextColor(sf::Color(255, 255, 255));
			lbl_sound->setTextSize(25);
			
			// Create Sound Slider
			tgui::Slider::Ptr slider_sound(gui);
			slider_sound->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			slider_sound->setVerticalScroll(false);
			slider_sound->setMinimum(0);
			slider_sound->setMaximum(100);
			slider_sound->setValue(setting.sound_volume());
			
			// Create Back button
			tgui::Button::Ptr btn_back(gui);
			btn_back->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			btn_back->setSize(260, 60);
			btn_back->setText("Back");
			btn_back->setTextSize(25);
			btn_back->bindCallback(tgui::Button::LeftMouseClicked);
			btn_back->setCallbackId(1);
			 
			// Main loop
			while (window.isOpen())
			{
				sf::Event event;
				while (window.pollEvent(event))
				{
					if (event.type == sf::Event::Closed)
						window.close();
					
					// Pass the event to all the widgets
					gui.handleEvent(event);
				}
				
				// The callback loop
				tgui::Callback callback;
				while (gui.pollCallback(callback))
				{
					// Make sure tha callback comes from the button
					if(callback.id == 1)
					{
						sound.play(); 
						return;
					}
				}
				// Update widget's position
				pic_paper->setSize(setting.size_window().x,setting.size_window().y);
				pic_dragon->setSize(setting.size_window().x,setting.size_window().y);
				lbl_music->setPosition((setting.size_window().x/2)-100, (setting.size_window().y/6));
				slider_music->setPosition((setting.size_window().x/2)-102, setting.size_window().y/4);
				lbl_sound->setPosition((setting.size_window().x/2)-100, (setting.size_window().y/3));
				slider_sound->setPosition((setting.size_window().x/2)-102, setting.size_window().y/2.4);
				btn_back->setPosition((setting.size_window().x/2)-130, setting.size_window().y/1.5);
				
				setting.set_music_volume(slider_music->getValue());
				setting.set_sound_volume(slider_sound->getValue());
				
				music.setVolume(setting.music_volume());
				sound.setVolume(setting.sound_volume());
				
				window.clear();
				// Draw all created widgets
				gui.draw();
				setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
				window << setting.cursor();
				window.display();
				
			}
		}
	}
}
 
#endif
 