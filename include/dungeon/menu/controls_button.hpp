 // Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
 
 // Licensed under the Apache License, Version 2.0 (the "License");
 // you may not use this file except in compliance with the License.
 // You may obtain a copy of the License at
 // 
 // http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software
 // distributed under the License is distributed on an "AS IS" BASIS,
 // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
 #ifndef DUNGEON_RPG_SFML_CONTROLS_BUTTON_HPP
 #define DUNGEON_RPG_SFML_CONTROLS_BUTTON_HPP
 
 #include <TGUI/TGUI.hpp>
 #include <SFML/Audio.hpp>
 
 #include <hnc/sleep.hpp>
 
 #include "../setting.hpp"
 
 namespace dungeon
{
	 namespace menu
	 {
		 
		 inline void change_controls(dungeon::setting & setting, tgui::Button::Ptr & btn)
		 {
			bool key_pressed = false; 
			
			sf::Keyboard::Key key;
			
			int i = btn->getCallbackId();
			
			while(!key_pressed)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
				{
					setting.keys[i-1] = sf::Keyboard::A;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::A;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
				{
					setting.keys[i-1] = sf::Keyboard::Z;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Z;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::E))
				{
					setting.keys[i-1] = sf::Keyboard::E;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::E;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				{
					setting.keys[i-1] = sf::Keyboard::R;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::R;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::T))
				{
					setting.keys[i-1] = sf::Keyboard::T;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::T;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
				{
					setting.keys[i-1] = sf::Keyboard::Y;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Y;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::U))
				{
					setting.keys[i-1] = sf::Keyboard::U;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::U;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::I))
				{
					setting.keys[i-1] = sf::Keyboard::I;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::I;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{
					setting.keys[i-1] = sf::Keyboard::O;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::O;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::P))
				{
					setting.keys[i-1] = sf::Keyboard::P;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::P;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
				{
					setting.keys[i-1] = sf::Keyboard::Q;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Q;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
				{
					setting.keys[i-1] = sf::Keyboard::S;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::S;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
				{
					setting.keys[i-1] = sf::Keyboard::D;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::D;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::F))
				{
					setting.keys[i-1] = sf::Keyboard::F;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::F;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::G))
				{
					setting.keys[i-1] = sf::Keyboard::G;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::G;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::H))
				{
					setting.keys[i-1] = sf::Keyboard::H;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::H;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::J))
				{
					setting.keys[i-1] = sf::Keyboard::J;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::J;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::K))
				{
					setting.keys[i-1] = sf::Keyboard::K;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::K;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::L))
				{
					setting.keys[i-1] = sf::Keyboard::L;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::L;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::M))
				{
					setting.keys[i-1] = sf::Keyboard::M;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::M;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
				{
					setting.keys[i-1] = sf::Keyboard::W;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::W;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
				{
					setting.keys[i-1] = sf::Keyboard::X;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::X;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::C))
				{
					setting.keys[i-1] = sf::Keyboard::C;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::C;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::V))
				{
					setting.keys[i-1] = sf::Keyboard::V;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::V;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::B))
				{
					setting.keys[i-1] = sf::Keyboard::B;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::B;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::N))
				{
					setting.keys[i-1] = sf::Keyboard::N;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::N;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}

				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				{
					setting.keys[i-1] = sf::Keyboard::Left;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Left;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				{
					setting.keys[i-1] = sf::Keyboard::Right;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Right;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				{
					setting.keys[i-1] = sf::Keyboard::Up;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Up;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				{
					setting.keys[i-1] = sf::Keyboard::Down;
					if (setting.identical_key())
					{
						setting.keys[i-1] = sf::Keyboard::Unknown;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
					else
					{
						setting.keys[i-1] = sf::Keyboard::Down;
						btn->setText(setting.get_id_key(i-1));
						key_pressed = true;
					}
				}
				
			}
		 }
		 
		 /**
		  * @brief Controls Button 
		  * 
		  * @code
		  * #include <TGUI/TGUI.hpp>
		  * #include <SFML/Audio.hpp>
		  * #include "../setting.hpp"
		  * @endcode
		  * 
		  * @param[in] window Window of the game
		  */
		 inline void controls_button( sf::RenderWindow & window, dungeon::setting & setting, sf::Sound & sound )
		 { 
			 tgui::Gui gui(window);
			 
			 // Load the font (you should check the return value to make sure that it is loaded)
			 gui.setGlobalFont("../media/police/Roman_SD.ttf");
			 
			 // Create background Paper picture
			 tgui::Picture::Ptr pic_paper(gui);
			 pic_paper->load("../media/texture/Lamoot/UI/Background/paper_background.png");
			 pic_paper->setSize(setting.size_window().x,setting.size_window().y);
			 
			 // Create background Dragon picture
			 tgui::Picture::Ptr pic_dragon(gui);
			 pic_dragon->load("../media/texture/Dungeon/dev/menu.png");
			 
			 // Create Left Label
			 tgui::Label::Ptr lbl_left(gui);
			 lbl_left->load("../media/texture/TGUI/widgets/Black.conf");
			 lbl_left->setText("Move Left");
			 lbl_left->setTextColor(sf::Color(255, 255, 255));
			 lbl_left->setTextSize(24);
			 
			 // Create Right Label
			 tgui::Label::Ptr lbl_right(gui);
			 lbl_right->load("../media/texture/TGUI/widgets/Black.conf");
			 lbl_right->setText("Move Right");
			 lbl_right->setTextColor(sf::Color(255, 255, 255));
			 lbl_right->setTextSize(24);
			 
			 // Create Up Label
			 tgui::Label::Ptr lbl_up(gui);
			 lbl_up->load("../media/texture/TGUI/widgets/Black.conf");
			 lbl_up->setText("Move Up");
			 lbl_up->setTextColor(sf::Color(255, 255, 255));
			 lbl_up->setTextSize(24);
			 
			 // Create Down Label
			 tgui::Label::Ptr lbl_down(gui);
			 lbl_down->load("../media/texture/TGUI/widgets/Black.conf");
			 lbl_down->setText("Move Down");
			 lbl_down->setTextColor(sf::Color(255, 255, 255));
			 lbl_down->setTextSize(24);
			 
			 // Create Back button
			 tgui::Button::Ptr btn_back(gui);
			 btn_back->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_back->setSize(260, 60);
			 btn_back->setText("Back");
			 btn_back->setTextSize(25);
			 btn_back->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_back->setCallbackId(5);
			 
			 // Create Left button
			 tgui::Button::Ptr btn_left(gui);
			 btn_left->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_left->setSize(195, 45);
			 btn_left->setText(setting.get_id_key(0));
			 btn_left->setTextSize(20);
			 btn_left->bindCallback([&]() -> void { change_controls(setting, btn_left); }, tgui::Button::LeftMouseClicked);
			 btn_left->setCallbackId(1);
			 
			 // Create Right button
			 tgui::Button::Ptr btn_right(gui);
			 btn_right->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_right->setSize(195, 45);
			 btn_right->setText(setting.get_id_key(1));
			 btn_right->setTextSize(20);
			 btn_right->bindCallback([&]() -> void { change_controls(setting, btn_right); }, tgui::Button::LeftMouseClicked);
			 btn_right->setCallbackId(2);
			 
			 // Create Up button
			 tgui::Button::Ptr btn_up(gui);
			 btn_up->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_up->setSize(195, 45);
			 btn_up->setText(setting.get_id_key(2));
			 btn_up->setTextSize(20);
			 btn_up->bindCallback([&]() -> void { change_controls(setting, btn_up); }, tgui::Button::LeftMouseClicked);
			 btn_up->setCallbackId(3);
			 
			 // Create Down button
			 tgui::Button::Ptr btn_down(gui);
			 btn_down->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_down->setSize(195, 45);
			 btn_down->setText(setting.get_id_key(3));
			 btn_down->setTextSize(20);
			 btn_down->bindCallback([&]() -> void { change_controls(setting, btn_down); }, tgui::Button::LeftMouseClicked);
			 btn_down->setCallbackId(4);
			 
			 // Main loop
			 while (window.isOpen())
			 {
				 sf::Event event;
				 while (window.pollEvent(event))
				 {
					 if (event.type == sf::Event::Closed)
						 window.close();
					 
					 // Pass the event to all the widgets
					 gui.handleEvent(event);
				 }
				 
				 // The callback loop
				 tgui::Callback callback;
				 while (gui.pollCallback(callback))
				 {
					 // Make sure tha callback comes from the button
					 switch(callback.id)
					 {
						 case 1: sound.play(); std::cout<<"Left"<<std::endl;break;
						 case 2: sound.play(); std::cout<<"Right"<<std::endl;break;
						 case 3: sound.play(); std::cout<<"Up"<<std::endl;break;
						 case 4: sound.play(); std::cout<<"Down"<<std::endl;break;
						 case 5: sound.play(); return; break;
					 }
				 }
				 // Update widget's position
				 pic_paper->setSize(setting.size_window().x,setting.size_window().y);
				 pic_dragon->setSize(setting.size_window().x,setting.size_window().y);
				 lbl_left->setPosition(setting.size_window().x/4, setting.size_window().y/5.4);
				 btn_left->setPosition((setting.size_window().x/2.7)+97.5, setting.size_window().y/6);
				 lbl_right->setPosition(setting.size_window().x/4, setting.size_window().y/3.7);
				 btn_right->setPosition((setting.size_window().x/2.7)+97.5, setting.size_window().y/4);
				 lbl_up->setPosition(setting.size_window().x/4, setting.size_window().y/2.8);
				 btn_up->setPosition((setting.size_window().x/2.7)+97.5, setting.size_window().y/3);
				 lbl_down->setPosition(setting.size_window().x/4, setting.size_window().y/2.3);
				 btn_down->setPosition((setting.size_window().x/2.7)+97.5, setting.size_window().y/2.4);
				 btn_back->setPosition((setting.size_window().x/2)-130, setting.size_window().y/1.5);
				 
				 window.clear();
				 // Draw all created widgets
				 gui.draw();
				 setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
				 window << setting.cursor();
				 window.display();
				 
			 }
		 }
	 }
 }
 
 #endif
 
