 // Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
 
 // Licensed under the Apache License, Version 2.0 (the "License");
 // you may not use this file except in compliance with the License.
 // You may obtain a copy of the License at
 // 
 // http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software
 // distributed under the License is distributed on an "AS IS" BASIS,
 // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
 #ifndef DUNGEON_RPG_SFML_GRAPHICS_BUTTON_HPP
 #define DUNGEON_RPG_SFML_GRAPHICS_BUTTON_HPP
 
 #include <list>

 #include <TGUI/TGUI.hpp>
 #include <SFML/Audio.hpp>
 #include "../setting.hpp"
 
 namespace dungeon
 {
	 namespace menu
	 {
		 /**
		  * @brief Load Button
		  * 
		  * @code
		  * #include <TGUI/TGUI.hpp>
		  * #include <SFML/Audio.hpp>
		  * #include "../setting.hpp"
		  * @endcode
		  * 
		  * @param[in] window Window of the game
		  */
		 inline void graphics_button( sf::RenderWindow & window, dungeon::setting & setting, sf::Sound & sound)
		 { 
			 tgui::Gui gui(window);
			 
			 // Load the font (you should check the return value to make sure that it is loaded)
			 gui.setGlobalFont("../media/police/Roman_SD.ttf");
			 
			 // Create background Paper picture
			 tgui::Picture::Ptr pic_paper(gui);
			 pic_paper->load("../media/texture/Lamoot/UI/Background/paper_background.png");
			 
			 // Create background Dragon picture
			 tgui::Picture::Ptr pic_dragon(gui);
			 pic_dragon->load("../media/texture/Dungeon/dev/menu.png");
			 
			 // Create Screen Size Label
			 tgui::Label::Ptr lbl_size(gui);
			 lbl_size->load("../media/texture/TGUI/widgets/Black.conf");
			 lbl_size->setText("Resolution");
			 lbl_size->setTextColor(sf::Color(255, 255, 255));
			 lbl_size->setTextSize(25);
			 
			 // Create Screen Size ComboBox
			 tgui::ComboBox::Ptr cbo_size(gui);
			 cbo_size->load("../media/texture/TGUI/widgets/Black.conf");
			 cbo_size->setSize(200, 21);
			 std::vector<sf::VideoMode> modes;

			 for(std::size_t i = 0; i < sf::VideoMode::getFullscreenModes().size(); i++)
			 {
			 	bool exist = false;
				for (std::size_t j = 0; j < modes.size(); j++)
				 {
				 	if(sf::VideoMode::getFullscreenModes()[i].width == modes[j].width && sf::VideoMode::getFullscreenModes()[i].height == modes[j].height)
				 	{
				 		exist = true;
				 	}
				 }
				 if(!exist)
				 {
				 	modes.push_back(sf::VideoMode::getFullscreenModes()[i]);
				 }
			 }

			 std::cout << modes.size() << std::endl;

			 for (std::size_t i = 0; i < modes.size(); ++i)
			 {
			 	cbo_size->addItem(std::to_string(modes[i].width) + " x " + std::to_string(modes[i].height), i);
			 }
			 cbo_size->bindCallback(tgui::ComboBox::ItemSelected);
			 cbo_size->setCallbackId(2);
			 
			 // Create FullScreen CheckBox
			 tgui::Checkbox::Ptr chk_fullscreen(gui);
			 chk_fullscreen->load("../media/texture/Lamoot/UI/CheckBox/Menu.conf");
			 chk_fullscreen->setPosition((setting.size_window().x/2)-15, setting.size_window().y/2);
			 chk_fullscreen->setText("FullScreen");
			 chk_fullscreen->setSize(32, 32);
             chk_fullscreen->disable();
			 
			 // Create Back button
			 tgui::Button::Ptr btn_back(gui);
			 btn_back->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_back->setSize(260, 60);
			 btn_back->setText("Back");
			 btn_back->setTextSize(25);
			 btn_back->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_back->setCallbackId(1);

			cbo_size->setSelectedItem(std::to_string(setting.size_window().x) + " x " + std::to_string(setting.size_window().y));
			 
			 // Main loop
			 while (window.isOpen())
			 {
				 sf::Event event;
				 while (window.pollEvent(event))
				 {
					 if (event.type == sf::Event::Closed)
						 window.close();
					 else if (event.type == sf::Event::KeyPressed)
					{
						if (event.key.code == sf::Keyboard::Escape)
						{
							
						}
					}
					 // Pass the event to all the widgets
					 gui.handleEvent(event);
				 }
				 
				 // The callback loop
				 tgui::Callback callback;
				 while (gui.pollCallback(callback))
				 {
					 // Make sure tha callback comes from the button
					 if(callback.id == 1)
					 {
						 sound.play(); 
						 return;
					 }
					 else if (callback.id == 2)
					 {
					 	std::cout << "TOTO" << std::endl;
					 	if(!chk_fullscreen->isChecked() && !setting.fullscreen())
						{
							window.create(sf::VideoMode(modes[cbo_size->getSelectedItemIndex()].width, modes[cbo_size->getSelectedItemIndex()].height), "Dungeon RPG SFML");
							window.setMouseCursorVisible(false);
							setting.set_size_window(sf::Vector2u(modes[cbo_size->getSelectedItemIndex()].width, modes[cbo_size->getSelectedItemIndex()].height));
						}
									 
						else if (!chk_fullscreen->isChecked() && setting.fullscreen())
						{		 
							window.create(sf::VideoMode(setting.size_window().x, setting.size_window().y), "Dungeon RPG SFML");
							window.setMouseCursorVisible(false);
							setting.set_no_fullscreen();
						}
									 
						if (chk_fullscreen->isChecked() && !setting.fullscreen())
						{
							window.create(sf::VideoMode(setting.size_window().x, setting.size_window().y), "Dungeon RPG SFML", sf::Style::Fullscreen);
							window.setMouseCursorVisible(false);
							setting.set_fullscreen();
						}
					 }
				 }

				if (!chk_fullscreen->isChecked() && setting.fullscreen())
				{		 
					window.create(sf::VideoMode(800, 600), "Dungeon RPG SFML");
					window.setMouseCursorVisible(false);
					setting.set_no_fullscreen();
				}
									 
				else if (chk_fullscreen->isChecked() && !setting.fullscreen())
				{
					window.create(sf::VideoMode(800, 600), "Dungeon RPG SFML", sf::Style::Fullscreen);
					window.setMouseCursorVisible(false);
					setting.set_fullscreen();
				}

				 // Update widget's position
				 pic_paper->setSize(setting.size_window().x,setting.size_window().y);
				 pic_dragon->setSize(setting.size_window().x,setting.size_window().y);
				 lbl_size->setPosition((setting.size_window().x/2)-100, (setting.size_window().y/4)+10);
				 cbo_size->setPosition((setting.size_window().x/2)-100, setting.size_window().y/3);
				 chk_fullscreen->setPosition((setting.size_window().x/2)-100, setting.size_window().y/2);
				 btn_back->setPosition((setting.size_window().x/2)-130, setting.size_window().y/1.5);
				 
				 window.clear();
				 // Draw all created widgets
				 gui.draw();
				 setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
				 window << setting.cursor();
				 window.display();
				 
			 }
		 }
	 }
 }
 
 #endif
 
