 // Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
 
 // Licensed under the Apache License, Version 2.0 (the "License");
 // you may not use this file except in compliance with the License.
 // You may obtain a copy of the License at
 // 
 // http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software
 // distributed under the License is distributed on an "AS IS" BASIS,
 // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 // See the License for the specific language governing permissions and
 // limitations under the License.
 
 #ifndef DUNGEON_RPG_SFML_OPTION_BUTTON_HPP
 #define DUNGEON_RPG_SFML_OPTION_BUTTON_HPP
 
 #include <TGUI/TGUI.hpp>
 #include <SFML/Audio.hpp>
 
 #include "audio_button.hpp"
 #include "graphics_button.hpp"
 #include "controls_button.hpp"
 #include "../setting.hpp"
 
 
 namespace dungeon
 {
	 namespace menu
	 {
		 /**
		  * @brief Option's Selection 
		  * 
		  * @code
		  * #include <TGUI/TGUI.hpp>
		  * #include <SFML/Audio.hpp>
		  * #include "audio_button.hpp"
		  * # include "graphics_button*.hpp"
		  * #include "controls_button.hpp"
		  * #include "../setting.hpp"
		  * @endcode
		  * 
		  * @param[in] window Window of the game
		  */
		 inline void option_button(sf::RenderWindow & window, dungeon::setting & setting, sf::Music & music, sf::Sound & sound)
		 { 
			 tgui::Gui gui(window);
			 
			 // Load the font (you should check the return value to make sure that it is loaded)
			 gui.setGlobalFont("../media/police/Roman_SD.ttf");
			 
			 // Create background Paper picture
			 tgui::Picture::Ptr pic_paper(gui);
			 pic_paper->load("../media/texture/Lamoot/UI/Background/paper_background.png");
			 
			 // Create background Dragon picture
			 tgui::Picture::Ptr pic_dragon(gui);
			 pic_dragon->load("../media/texture/Dungeon/dev/menu.png");
			 
			 // Create Graphics button
			 tgui::Button::Ptr btn_graphics(gui);
			 btn_graphics->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_graphics->setSize(260, 60);
			 btn_graphics->setText("Graphics");
			 btn_graphics->setTextSize(25);
			 btn_graphics->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_graphics->setCallbackId(1);
			 
			 // Create Audio button
			 tgui::Button::Ptr btn_audio(gui);
			 btn_audio->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_audio->setSize(260, 60);
			 btn_audio->setText("Audio");
			 btn_audio->setTextSize(25);
			 btn_audio->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_audio->setCallbackId(2);
			 
			 // Create Controls button
			 tgui::Button::Ptr btn_controls(gui);
			 btn_controls->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_controls->setSize(260, 60);
			 btn_controls->setText("Controls");
			 btn_controls->setTextSize(25);
			 btn_controls->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_controls->setCallbackId(3);
			 
			 // Create the Back button
			 tgui::Button::Ptr btn_back(gui);
			 btn_back->load("../media/texture/Lamoot/UI/Button/Menu.conf");
			 btn_back->setSize(260, 60);
			 btn_back->setText("Back");
			 btn_back->setTextSize(25);
			 btn_back->bindCallback(tgui::Button::LeftMouseClicked);
			 btn_back->setCallbackId(4);
			 
			 
			 // Main loop
			 while (window.isOpen())
			 {
				 sf::Event event;
				 while (window.pollEvent(event))
				 {
					 if (event.type == sf::Event::Closed)
						 window.close();
					 
					 // Pass the event to all the widgets
					 gui.handleEvent(event);
				 }
				 
				 // The callback loop
				 tgui::Callback callback;
				 while (gui.pollCallback(callback))
				 {
					 // Make sure tha callback comes from the button
					 switch(callback.id)
					 {
						 case 1: sound.play(); dungeon::menu::graphics_button(window, setting, sound); break;
						 case 2: sound.play(); dungeon::menu::audio_button(window, setting, music, sound); break;
						 case 3: sound.play(); dungeon::menu::controls_button(window, setting, sound);break;
						 case 4: sound.play(); return; break;
					 }
				 }
				 // Update widget's position
				 pic_paper->setSize(setting.size_window().x,setting.size_window().y);
				 pic_dragon->setSize(setting.size_window().x, setting.size_window().y);
				 btn_graphics->setPosition((setting.size_window().x/2)-130, setting.size_window().y/6);
				 btn_audio->setPosition((setting.size_window().x/2)-130, setting.size_window().y/3);
				 btn_controls->setPosition((setting.size_window().x/2)-130, setting.size_window().y/2);
				 btn_back->setPosition((setting.size_window().x/2)-130, setting.size_window().y/1.5);
				 
				 window.clear();
				 
				 // Draw all created widgets
				 gui.draw();
				 setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
				 window << setting.cursor();
				 window.display();
				 
			 }
		 }
	 }
 }
 
 #endif
 
 
