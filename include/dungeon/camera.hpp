// Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_CAMERA_HPP
#define DUNGEON_RPG_SFML_CAMERA_HPP

#include <algorithm>

#include <SFML/Graphics.hpp>

#include "npc.hpp"
#include "map.hpp"
#include "setting.hpp"

namespace dungeon 
{
	/**
	 * @brief Camera
	 * 
	 * @code
	   #include <dungeon/npc.hpp>
	   @endcode
	 * 
	 */
	
	class camera
	{
	private:
	
		/// View
		sf::View m_view;
		
		///Window
		float m_window_height;
		float m_window_width;
		
		///Background
		float m_background_height;
		float m_background_width;
		
	public:
		camera(dungeon::setting & setting, dungeon::map & map): 
		m_window_width(setting.size_window().x), 
		m_window_height(setting.size_window().y),
		m_background_width(map.height()), 
		m_background_height(map.width())
		{ }
		/// @brief View of the Hero 
		/// @param[in] dungeon::hero j1 	The Hero
		void view(dungeon::hero & j1)
		{
			m_view.reset(sf::FloatRect(0,0,m_window_width, m_window_height));
			sf::Vector2f position(m_window_width/2, m_window_height/2);
			sf::Vector2f cursor(m_window_width/2, m_window_height/2);
			
			if (m_background_height > m_window_height)
			{
			position.x = j1.get_hero().m_sprite.position().x -(m_window_width/2);
			position.y = j1.get_hero().m_sprite.position().y -(m_window_height/2);
			
			position.x = std::max(position.x, 0.f);
			position.y = std::max(position.y, 0.f);
			position.x = std::min(position.x, (m_background_width)-m_window_width);
			position.y = std::min(position.y, (m_background_height)-m_window_height);
	
			m_view.reset(sf::FloatRect(position.x, position.y, m_window_width, m_window_height));
			}
		}
    
		/// @brief Return the view of the Hero
		/// @return m_view the view of the Hero
		sf::View const & getView() const
		{
			return m_view;
		}
   };
}


#endif
