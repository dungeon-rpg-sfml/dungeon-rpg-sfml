// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

#ifndef DUNGEON_RPG_SFML_TELEPORTER_HPP
#define DUNGEON_RPG_SFML_TELEPORTER_HPP

#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>

#include <thoth/sprite_centered.hpp>
#include <thoth/textures.hpp>

#include <dungeon/hero.hpp>


namespace dungeon 
{
	/**
	 * @brief Teleporter
	 * 
	 * @code
	   #include <thoth/sprite_centered.hpp>
	   #include <thoth/texture.hpp>
	   @endcode
	 * 
	 */
	
	class teleporter
	{
	private:
		
		
	public:
		/// Animated spite of Teleporter
		thoth::sprite_animated m_anim;
		
		thoth::sprite_centered m_sprite;
		
		teleporter(){ }
		
		teleporter(thoth::texture const & default_texture) : m_anim(default_texture)
		{ }
		
	};
	
	sf::RenderWindow & operator<<(sf::RenderWindow & window, dungeon::teleporter const & teleporter)
	{
		window << teleporter.m_anim;
		return  window;
	}
}





#endif
