// Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_MENU_WIDGETS_COMBAT_HPP
#define DUNGEON_RPG_SFML_MENU_WIDGETS_COMBAT_HPP

#include <TGUI/TGUI.hpp>
#include <SFML/Audio.hpp>

#include "setting.hpp"

namespace dungeon
{
	namespace combat
	{
		/**
		 * @brief Load widgets for main menu
		 * 
		 * @code
		 *		  #include <TGUI/TGUI.hpp>
		 *		  @endcode
		 * 
		 * @param[in] gui A tgui::Gui
		 */
		inline void widgets_combat(dungeon::setting & setting, tgui::Gui & gui, dungeon::npc & p1, dungeon::npc & p2)
		{
			// Load the font (you should check the return value to make sure that it is loaded)
			gui.setGlobalFont("../media/police/Roman_SD.ttf");
			
			// Create background image
			tgui::Picture::Ptr picture(gui);
			picture->load("../media/texture/Dungeon/dev/battle_back.png");
			picture->setSize(setting.size_window().x, setting.size_window().y);
			
			// Create background image
			tgui::Picture::Ptr picture2(gui);
			picture2->load("../media/texture/Dungeon/dev/battle_back2.png");
			picture2->setSize(setting.size_window().x, setting.size_window().y);
			
// 			tgui::Panel::Ptr panel1(gui, "first panel");

			
			// Create  P1 name Label
			tgui::Label::Ptr name_p1(gui);
			name_p1->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			name_p1->setText("Palindor");
			name_p1->setPosition(0, 0);
			name_p1->setTextColor(sf::Color(255, 255, 255));
			name_p1->setTextSize(24);
			
			
			// Create P2 name Label
			tgui::Label::Ptr name_p2(gui);
			name_p2->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			name_p2->setText("Toto");
			name_p2->setPosition(700, 0);
			name_p2->setTextColor(sf::Color(255, 255, 255));
			name_p2->setTextSize(24);
			
		}
	}
}

#endif
