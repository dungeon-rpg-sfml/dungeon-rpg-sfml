// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2014 Kevin Vinchon, kevin.vinchon@u-psud.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_MISC_HPP
#define DUNGEON_RPG_SFML_MISC_HPP

#include <hnc/geometry.hpp>

#include <SFML/Graphics.hpp>

#include <thoth/sprite_centered.hpp>
#include <thoth/textures.hpp>

namespace dungeon
{
	/**
	 * @brief Misc
	 * 
	 * @code
	   #include <thoth/sprite_centered.hpp>
	   #include <thoth/texture.hpp>
	   @endcode
	 * 
	 */
	class misc
	{
	public:
		
		///Misc's Sprite
		thoth::sprite_centered m_sprite;
		
		/// @brief Constructor
		/// @param[in] texture        Texture of the misc
		misc(thoth::texture const & default_texture) : 
		m_sprite(default_texture)
		{ }
		
		/// @brief Constructor
		misc() 
		{ }
	
	};
	
	sf::RenderWindow & operator<<(sf::RenderWindow & window, dungeon::misc const & misc)
	{
		window << misc.m_sprite;
		return  window;
	}
}

#endif
