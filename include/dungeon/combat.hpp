// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_COMBAT_HPP
#define DUNGEON_RPG_SFML_COMBAT_HPP

#include<hnc/random.hpp>
#include <hnc/sleep.hpp>

#include <thread>

#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <TGUI/TGUI.hpp>

#include "widget_combat.hpp"
#include "setting.hpp"

namespace dungeon
{
	
	/**
	 * @brief Provides menu functions
	 *
	 * @code
	 *	   #include <TGUI/TGUI.hpp>
	 *	   #include <SFML/Audio.hpp>
	 *	   @endcode
	 */
	namespace combat
	{
		
		void attack(sf::RenderWindow & window, dungeon::setting & setting, dungeon::npc & p1, dungeon::npc & p2)
		{
			/// Hit
			sf::SoundBuffer buffer1;
			buffer1.loadFromFile("../media/sound/Independent.nu/hits/hit05.ogg");
			sf::Sound hit;
			hit.setBuffer(buffer1);
			hit.setVolume(setting.sound_volume());
			
			/// Fail
			sf::SoundBuffer buffer2;
			buffer2.loadFromFile("../media/sound/Remaxim/3_melee_sounds/melee.ogg");
			sf::Sound fail;
			fail.setBuffer(buffer2);
			fail.setVolume(setting.sound_volume());
			
			/// Fatality
			sf::SoundBuffer buffer3;
			buffer3.loadFromFile("../media/sound/Independent.nu/hits/hit32.ogg");
			sf::Sound fatality;
			fatality.setBuffer(buffer3);
			fatality.setVolume(setting.sound_volume());
			
			while (window.isOpen())
			{
				
				int attack = hnc::random::uniform(1, 20);
				
				std::cout << "Test: " << attack << std::endl;
				
				if(attack <= p1.damage())
				{
					hit.play();
					p2.set_life(p2.life() - p1.damage());
					if(p2.life()<0)
					{
						p2.set_life(0);
					}
					std::cout << "Attaque réussie" << std::endl;
					std::cout << "Vie NPC " << p2.life() << std::endl;
				}
				else if (attack == 20)
				{
					fatality.play();
					p1.set_life(p1.life() - p1.damage());
					if(p1.life()<0)
					{
						p1.set_life(0);
					}
				}
				
				else 
				{
					fail.play();
					std::cout << "Attaque foirée" << std::endl;
					std::cout << "Vie " << p2.life() << std::endl;
				}
				hnc::sleep::s(1);
				
				return;
			}
		}
		
		void blood_path(sf::RenderWindow & window, dungeon::setting & setting, dungeon::npc & p1, dungeon::npc & p2)
		{
			/// succes
			sf::SoundBuffer buffer_s;
			buffer_s.loadFromFile("../media/sound/p0ss/spells/curse.ogg");
			sf::Sound succes;
			succes.setBuffer(buffer_s);
			succes.setVolume(setting.sound_volume());
			
			/// fail
			sf::SoundBuffer buffer_e;
			buffer_e.loadFromFile("../media/sound/p0ss/spells/zap7a.ogg");
			sf::Sound fail;
			fail.setBuffer(buffer_e);
			fail.setVolume(setting.sound_volume());
			
			/// fatality
			sf::SoundBuffer buffer_f;
			buffer_f.loadFromFile("../media/sound/qubodup/PowerDrain.ogg");
			sf::Sound fatality;
			fatality.setBuffer(buffer_f);
			fatality.setVolume(setting.sound_volume());
			
			while (window.isOpen())
			{
				
				int action = hnc::random::uniform(1, 20);
				
				std::cout << "Test: " << action << std::endl;
				
				if(action <= p1.intelligence())
				{
					succes.play();
					p1.set_mana(p1.mana() - 30);
					p1.set_life(p1.life() + 10);
					if(p1.life()>100)
					{
						p1.set_life(100);
					}
					p2.set_life(p2.life() - 10);
					if(p2.life()<0)
					{
						p2.set_life(0);
					}
				}
				else if (action == 20)
				{
					fatality.play();
					p1.set_mana(0);
					p2.set_life(p2.life() + 20);
					if(p2.life()>100)
					{
						p2.set_life(100);
					}
					p1.set_life(p1.life() - 10);
					if(p1.life()<0)
					{
						p1.set_life(0);
					}
				}
				
				else 
				{
					fail.play();
					p1.set_mana(p1.mana() - 30);
				}
				
				hnc::sleep::s(1);
				return;
			}
		}
		
		bool fist_of_justice(sf::RenderWindow & window, dungeon::setting & setting, dungeon::npc & p1, dungeon::npc & p2)
		{
			/// succes
			sf::SoundBuffer buffer_s;
			buffer_s.loadFromFile("../media/sound/p0ss/spells/explode4.ogg");
			sf::Sound succes;
			succes.setBuffer(buffer_s);
			succes.setVolume(setting.sound_volume());
			
			/// fail
			sf::SoundBuffer buffer_e;
			buffer_e.loadFromFile("../media/sound/p0ss/spells/zap7a.ogg");
			sf::Sound fail;
			fail.setBuffer(buffer_e);
			fail.setVolume(setting.sound_volume());
			
			/// fatality
			sf::SoundBuffer buffer_f;
			buffer_f.loadFromFile("../media/sound/qubodup/PowerDrain.ogg");
			sf::Sound fatality;
			fatality.setBuffer(buffer_f);
			fatality.setVolume(setting.sound_volume());
			
			while (window.isOpen())
			{
				
				int action = hnc::random::uniform(1, 20);
				
				std::cout << "Test: " << action << std::endl;
				
				if(action <= p1.intelligence())
				{
					succes.play();
					p1.set_mana(p1.mana() - 15);
					p2.set_damage(p1.damage() - p1.damage()/2);
					
					hnc::sleep::s(1);
					return true;
				}
				else if (action == 20)
				{
					fatality.play();
					p1.set_mana(p1.mana() - 15);
					p1.set_life(p1.life() - 10);
					if(p1.life()>100)
					{
						p1.set_life(100);
					}
					hnc::sleep::s(1);
					return false;
				}
				
				else 
				{
					fail.play();
					p1.set_mana(p1.mana() - 15);
					
					hnc::sleep::s(1);
					return false;
				}
			}
		}
		
		void heal(sf::RenderWindow & window, dungeon::setting & setting, dungeon::npc & p1)
		{
			
			/// Heal
			sf::SoundBuffer buffer;
			buffer.loadFromFile("../media/sound/p0ss/spells/blessing2.ogg");
			sf::Sound heal;
			heal.setBuffer(buffer);
			heal.setVolume(setting.sound_volume());
			
			/// fail
			sf::SoundBuffer buffer_e;
			buffer_e.loadFromFile("../media/sound/p0ss/spells/zap7a.ogg");
			sf::Sound fail;
			fail.setBuffer(buffer_e);
			fail.setVolume(setting.sound_volume());
			
			/// fatality
			sf::SoundBuffer buffer_f;
			buffer_f.loadFromFile("../media/sound/qubodup/PowerDrain.ogg");
			sf::Sound fatality;
			fatality.setBuffer(buffer_f);
			fatality.setVolume(setting.sound_volume());

			
			while (window.isOpen())
			{
			
				int action = hnc::random::uniform(1, 20);
				if(action <= p1.intelligence())
				{
					heal.play();
					p1.set_mana(p1.mana()-10);
					if(p1.life() + 10 > 100)
					{
						p1.set_life(100);
					}
					else
					{
						p1.set_life(p1.life() + 10);
					}
				}
				else if(action == 20)
				{
					fatality.play();
					p1.set_mana(p1.mana()/2);
				}
				
				else
				{
					fail.play();
					p1.set_mana(p1.mana() - 10);
				}
				
				hnc::sleep::s(1);
				return;
			}
		}
		
		bool fight(sf::RenderWindow & window, dungeon::setting & setting, dungeon::npc & p1, dungeon::npc & p2)
		{
			
			window.setMouseCursorVisible(false);
			sf::View view(sf::FloatRect(0, 0, setting.size_window().x, setting.size_window().y));
			
			sf::SoundBuffer buffer_click;
			buffer_click.loadFromFile("../media/sound/LFA/mouse_click/mouseclick.ogg");
			
			sf::Sound sound;
			sound.setBuffer(buffer_click);
			sound.setVolume(setting.sound_volume());
			
			
			sf::SoundBuffer buffer_p1;
			buffer_p1.loadFromFile("../media/sound/Brandon_Morris/quote_fight.ogg");
			
			sf::Sound sound_p1;
			sound_p1.setBuffer(buffer_p1);
			sound_p1.setVolume(setting.sound_volume());
			sound_p1.play();
			
			hnc::sleep::s(1);
			
			sf::Music music;
			music.openFromFile("../media/music/Jobromedia/battle_in_the_winter/battle_in_the_winter.ogg");
			music.setVolume(setting.music_volume());
			music.play();
			music.setLoop(true);
			
			sf::SoundBuffer buffer_p1_victory;
			buffer_p1_victory.loadFromFile("../media/sound/Brandon_Morris/laugh.ogg");
			sf::Sound sound_p1_victory;
			sound_p1_victory.setBuffer(buffer_p1_victory);
			sound_p1_victory.setVolume(setting.sound_volume());
			
			sf::SoundBuffer buffer_p1_lose;
			buffer_p1_lose.loadFromFile("../media/sound/thebardofblasphemy/death.ogg");
			sf::Sound sound_p1_lose;
			sound_p1_lose.setBuffer(buffer_p1_lose);
			sound_p1_lose.setVolume(setting.sound_volume());
			
			sf::Music victory;
			victory.openFromFile("../media/sound/Remaxim/win_music/win.ogg");
			victory.setVolume(setting.music_volume());
			
			sf::Music lose;
			lose.openFromFile("../media/sound/Remaxim/lose_music/lose.ogg");
			lose.setVolume(setting.music_volume());
			
			thoth::sprite a(p1.default_texture());
			thoth::sprite b(p2.default_texture());
			
			a.set_position((setting.size_window().x)/4, (setting.size_window().y)/2);
			b.set_position(3 * (setting.size_window().x)/4, (setting.size_window().y)/2);
			
			int tour = hnc::random::uniform(1, 2);;
			
			bool penality_p2 = false;
			bool penality_p1 = false;
			int fist_turn = 0;
			
			int save_damage_p1 = p1.damage();
			int save_damage_p2 = p2.damage();
			
			std::cout << tour << std::endl;
			
			tgui::Gui gui(window);
			
			// Load the font (you should check the return value to make sure that it is loaded)
			gui.setGlobalFont("../media/police/Roman_SD.ttf");
			
			// Create background image
			tgui::Picture::Ptr picture(gui);
			picture->load("../media/texture/Dungeon/dev/battle_back.png");
			picture->setSize(setting.size_window().x, setting.size_window().y);
			
			// Create background image
			tgui::Picture::Ptr picture2(gui);
			picture2->load("../media/texture/Dungeon/dev/battle_back2.png");
			picture2->setSize(setting.size_window().x, setting.size_window().y);
			
			
			// Create  P1 name Label
			tgui::Label::Ptr name_p1(gui);
			name_p1->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			name_p1->setText(p1.name());
			name_p1->setPosition(0, 0);
			name_p1->setTextColor(sf::Color(255, 255, 255));
			name_p1->setTextSize(24);
			
			tgui::LoadingBar::Ptr p1_life_bar(gui);
			p1_life_bar->load("../media/texture/Jannax/life.conf");
			p1_life_bar->setPosition(0, 50);
			p1_life_bar->setSize(300, 30);
			p1_life_bar->setValue(p1.life());
			
			tgui::LoadingBar::Ptr p1_mana_bar(gui);
			p1_mana_bar->load("../media/texture/Jannax/magicka.conf");
			p1_mana_bar->setPosition(0, p1_life_bar->getPosition().y + 30);
			p1_mana_bar->setSize(300, 30);
			p1_mana_bar->setValue(p1.mana());
			
			
			// Create P2 name Label
			tgui::Label::Ptr name_p2(gui);
			name_p2->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			name_p2->setText(p2.name());
			name_p2->setPosition(setting.size_window().x - 300, 0);
			name_p2->setTextColor(sf::Color(255, 255, 255));
			name_p2->setTextSize(24);
			
			tgui::LoadingBar::Ptr p2_life_bar(gui);
			p2_life_bar->load("../media/texture/Jannax/life.conf");
			p2_life_bar->setPosition(setting.size_window().x - 300, 50);
			p2_life_bar->setSize(300, 30);
			p2_life_bar->setValue(p2.life());
			
			tgui::LoadingBar::Ptr p2_mana_bar(gui);
			p2_mana_bar->load("../media/texture/Jannax/magicka.conf");
			p2_mana_bar->setPosition(setting.size_window().x - 300, p2_life_bar->getPosition().y + 30);
			p2_mana_bar->setSize(300, 30);
			p2_mana_bar->setValue(p2.mana());
			
			// Create Turn info Label
			tgui::Label::Ptr p1_tour(gui);
			p1_tour->load("../media/texture/Lamoot/UI/Slider/Menu.conf");
			p1_tour->setPosition(setting.size_window().x/4, setting.size_window().y - 30);
			p1_tour->setTextColor(sf::Color(255, 255, 255));
			p1_tour->setTextSize(24);
			
			// Create the Down button
			tgui::Button::Ptr btn_attack(gui);
			btn_attack->load("../media/texture/Lorc/attack.conf");
			btn_attack->setSize(100, 100);
			btn_attack->setPosition(setting.size_window().x/4, setting.size_window().y - 150);
			btn_attack->setText("    Attack    ");
			btn_attack->bindCallback(tgui::Button::LeftMouseClicked);
			btn_attack->setCallbackId(1);
			
			
			// Create the Down button
			tgui::Button::Ptr btn_heal(gui);
			btn_heal->load("../media/texture/Lorc/health.conf");
			btn_heal->setSize(100, 100);
			btn_heal->setPosition(btn_attack->getPosition().x + 100, setting.size_window().y - 150);
			btn_heal->setText("    HEAL    ");
			btn_heal->bindCallback(tgui::Button::LeftMouseClicked);
			btn_heal->setCallbackId(2);
			
			// Create the Down button
			tgui::Button::Ptr btn_fist_justice(gui);
			btn_fist_justice->load("../media/texture/Lorc/fist.conf");
			btn_fist_justice->setSize(100, 100);
			btn_fist_justice->setPosition(btn_heal->getPosition().x + 100, setting.size_window().y - 150);
			btn_fist_justice->setText("Fist of Justice");
			btn_fist_justice->bindCallback(tgui::Button::LeftMouseClicked);
			btn_fist_justice->setCallbackId(3);
			
			// Create the Down button
			tgui::Button::Ptr btn_blood_path(gui);
			btn_blood_path->load("../media/texture/Lorc/blood.conf");
			btn_blood_path->setSize(100, 100);
			btn_blood_path->setPosition(btn_fist_justice->getPosition().x + 100, setting.size_window().y - 150);
			btn_blood_path->setText("Fist of Justice");
			btn_blood_path->bindCallback(tgui::Button::LeftMouseClicked);
			btn_blood_path->setCallbackId(4);
			
			// Main loop
			while (window.isOpen())
			{
				sf::Event event;
				while (window.pollEvent(event))
				{
					if (event.type == sf::Event::Closed)
						window.close();
					
					// Key released
					else if (event.type == sf::Event::KeyReleased)
					{
						if (event.key.code == sf::Keyboard::Escape)
						{
							window.close();
						}
						else if (event.key.code == sf::Keyboard::V)
						{
							std::cout << "Penality P2: " << penality_p2 << std::endl;
							std::cout << "Turn: " << fist_turn << std::endl;
						}
					}
					
					// Pass the event to all the widgets
					gui.handleEvent(event);
				}
				
				window.setView(view);
				
				/// Player 1 Turn
				if (tour == 1)
				{
					
					p1_tour->setText("Turn: Player 1");
					
					/// Update Window
					window.clear();
					gui.draw();
					window << a;
					window << b;
					setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					window << setting.cursor();
					window.display();
					
					// The callback loop
					tgui::Callback callback;
					while (gui.pollCallback(callback))
					{
						// Make sure tha callback comes from the button
						switch(callback.id)
						{
							case 1: sound.play(); dungeon::combat::attack(window, setting, p1, p2); tour = 2;break;
							case 2: sound.play(); if(p1.mana() >= 10){dungeon::combat::heal(window, setting, p1); tour = 2;} 
												  else {}; break;
							case 3: sound.play(); if(p1.mana() >= 15 && !penality_p2 && fist_turn == 0){ std::cout << "Ok" << std::endl; penality_p2 = dungeon::combat::fist_of_justice(window, setting, p1, p2); tour = 2; }
												  else {}; break;
							case 4: sound.play(); if(p1.mana() >= 30){dungeon::combat::blood_path(window, setting, p1, p2); tour = 2;} 
												  else {}; break;
						}
					}
					
					/// Update Life and Mana bar
					p1_life_bar->setValue(p1.life());
					p1_mana_bar->setValue(p1.mana());
					p2_life_bar->setValue(p2.life());
					p2_mana_bar->setValue(p2.mana());
				}
				
				if (p2.life() <= 0)
				{
					music.pause();
					victory.play();
					sound_p1_victory.play();
					p1_tour->setText("YOU WIN !!!");
				
					/// Update Window
					window.clear();
					gui.draw();
					window << a;
					window << b;
					setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					window << setting.cursor();
					window.display();
					
					hnc::sleep::s(9);
					return true;
				}
				
				/// Player 2 Turn
				if (tour == 2)
				{
					p1_tour->setText("Turn: Player 2");
					
					if (penality_p2)
					{
						fist_turn++;
					}
					if (fist_turn == 3)
					{
						penality_p2 = false;
						fist_turn = 0;
						p2.set_damage(save_damage_p2);
					}
					
					/// Update Window
					window.clear();
					gui.draw();
					window << a;
					window << b;
					setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					window << setting.cursor();
					window.display();
					dungeon::combat::attack(window, setting, p2, p1);
					
					/// Update Life and Mana bar
					p1_life_bar->setValue(p1.life());
					p1_mana_bar->setValue(p1.mana());
					p2_life_bar->setValue(p2.life());
					p2_mana_bar->setValue(p2.mana());
					
					tour = 1;
				}
				
				if (p1.life() <= 0)
				{
					music.pause();
					lose.play();
					sound_p1_lose.play();
					p1_tour->setText("YOU LOSE !!!");
					
					/// Update Window
					window.clear();
					gui.draw();
					window << a;
					window << b;
					setting.cursor().set_position(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
					window << setting.cursor();
					window.display();
					
					hnc::sleep::s(9);
					return false;
				}
				
			}
			
		}
		
	}
}

#endif

