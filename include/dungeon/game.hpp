// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_GAME_HPP
#define DUNGEON_RPG_SFML_GAME_HPP

#include <SFML/Graphics.hpp>

#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <hnc/serialization.hpp>

#include <thoth/collision.hpp>

#include "npc.hpp"
#include "hero.hpp"
#include "camera.hpp"
#include "map.hpp"
#include "misc.hpp"
#include "setting.hpp"
#include "placard.hpp"
#include "combat.hpp"


namespace dungeon
{
	/**
	 * @brief Game
	 * 
	 * @code
	   #include <SFML/Graphics.hpp>
	   #include "npc.hpp"
	   #include "hero.hpp"
	   #include "camera.hpp"
	   #include "menu.hpp"
	   #include "map.hpp"
	   #include "misc.hpp"
	   @endcode
	 * 
	 */
	
	class game 
	{
	private:
		
		/// Camera's of Players 
		std::vector <dungeon::camera> m_cams;
		
		/// Players
		std::vector <dungeon::hero> m_heroes; 
		
		/// Maps
		std::vector <dungeon::map> m_maps;
		
		/// Window of the game
		sf::RenderWindow m_window;
		
		/// Setting
		dungeon::setting m_setting;

	public:
		
        game(thoth::texture const & texture) :
		m_setting(texture)
		{ 
			m_window.create(sf::VideoMode(800, 600), "Dungeon RPG SFML");
			m_window.setMouseCursorVisible(false);
		}
		
		void run()
		{
			
			/// Palindor's Textures
			auto const & t0_0 = thoth::textures().load_extern("t0_0", "../media/texture/Kazzador/npc/VXHeroA/hero_0-0.png");
			auto const & t0_1 = thoth::textures().load_extern("t0_1", "../media/texture/Kazzador/npc/VXHeroA/hero_0-1.png");
			auto const & t0_2 = thoth::textures().load_extern("t0_2", "../media/texture/Kazzador/npc/VXHeroA/hero_0-2.png");
			auto const & t1_0 = thoth::textures().load_extern("t1_0", "../media/texture/Kazzador/npc/VXHeroA/hero_1-0.png");
			auto const & t1_1 = thoth::textures().load_extern("t1_1", "../media/texture/Kazzador/npc/VXHeroA/hero_1-1.png");
			auto const & t1_2 = thoth::textures().load_extern("t1_2", "../media/texture/Kazzador/npc/VXHeroA/hero_1-2.png");
			auto const & t2_0 = thoth::textures().load_extern("t2_0", "../media/texture/Kazzador/npc/VXHeroA/hero_2-0.png");
			auto const & t2_1 = thoth::textures().load_extern("t2_1", "../media/texture/Kazzador/npc/VXHeroA/hero_2-1.png");
			auto const & t2_2 = thoth::textures().load_extern("t2_2", "../media/texture/Kazzador/npc/VXHeroA/hero_2-2.png");
			auto const & t3_0 = thoth::textures().load_extern("t3_0", "../media/texture/Kazzador/npc/VXHeroA/hero_3-0.png");
			auto const & t3_1 = thoth::textures().load_extern("t3_1", "../media/texture/Kazzador/npc/VXHeroA/hero_3-1.png");
			auto const & t3_2 = thoth::textures().load_extern("t3_2", "../media/texture/Kazzador/npc/VXHeroA/hero_3-2.png");
			
			
			dungeon::hero p1(t0_0, "Palindor", 30.f, 11.f, 12.f, 13.f);
			p1.set_setting(m_setting);
			
			/// Palindor's Quotes
			sf::SoundBuffer buffer_palindor_1;
			buffer_palindor_1.loadFromFile("../media/sound/Brandon_Morris/listentome.ogg");
			sf::Sound palindor_quote_1;
			palindor_quote_1.setBuffer(buffer_palindor_1);
			palindor_quote_1.setVolume(m_setting.sound_volume());
			
			sf::SoundBuffer buffer_palindor_2;
			buffer_palindor_2.loadFromFile("../media/sound/Brandon_Morris/preparetodie.ogg");
			sf::Sound palindor_quote_2;
			palindor_quote_2.setBuffer(buffer_palindor_2);
			palindor_quote_2.setVolume(m_setting.sound_volume());
			
			sf::SoundBuffer buffer_palindor_3;
			buffer_palindor_3.loadFromFile("../media/sound/Brandon_Morris/YOURfeeble.ogg");
			sf::Sound palindor_quote_3;
			palindor_quote_3.setBuffer(buffer_palindor_3);
			palindor_quote_3.setVolume(m_setting.sound_volume());
			
			/// Aela's Textures
			auto const & npcB0_0 = thoth::textures().load_extern("npcB0_0", "../media/texture/Kazzador/npc/VXNPC-B/npcB_0-0.png");
			auto const & npcB0_1 = thoth::textures().load_extern("npcB0_1", "../media/texture/Kazzador/npc/VXNPC-B/npcB_0-1.png");
			auto const & npcB0_2 = thoth::textures().load_extern("npcB0_2", "../media/texture/Kazzador/npc/VXNPC-B/npcB_0-2.png");
			auto const & npcB1_0 = thoth::textures().load_extern("npcB1_0", "../media/texture/Kazzador/npc/VXNPC-B/npcB_1-0.png");
			auto const & npcB1_1 = thoth::textures().load_extern("npcB1_1", "../media/texture/Kazzador/npc/VXNPC-B/npcB_1-1.png");
			auto const & npcB1_2 = thoth::textures().load_extern("npcB1_2", "../media/texture/Kazzador/npc/VXNPC-B/npcB_1-2.png");
			auto const & npcB2_0 = thoth::textures().load_extern("npcB2_0", "../media/texture/Kazzador/npc/VXNPC-B/npcB_2-0.png");
			auto const & npcB2_1 = thoth::textures().load_extern("npcB2_1", "../media/texture/Kazzador/npc/VXNPC-B/npcB_2-1.png");
			auto const & npcB2_2 = thoth::textures().load_extern("npcB2_2", "../media/texture/Kazzador/npc/VXNPC-B/npcB_2-2.png");
			auto const & npcB3_0 = thoth::textures().load_extern("npcB3_0", "../media/texture/Kazzador/npc/VXNPC-B/npcB_3-0.png");
			auto const & npcB3_1 = thoth::textures().load_extern("npcB3_1", "../media/texture/Kazzador/npc/VXNPC-B/npcB_3-1.png");
			auto const & npcB3_2 = thoth::textures().load_extern("npcB3_2", "../media/texture/Kazzador/npc/VXNPC-B/npcB_3-2.png");
			
			dungeon::npc aela(npcB0_0, "Aela");
			aela.m_sprite.add("left", 0.2f, { npcB1_1, npcB1_0, npcB1_2, npcB1_0 });
			aela.m_sprite.add("right", 0.2f, { npcB2_1,  npcB2_0, npcB2_2, npcB2_0 });
			aela.m_sprite.add("up", 0.2f, { npcB3_1, npcB3_0, npcB3_2, npcB3_0 });
			aela.m_sprite.add("down", 0.2f ,{ npcB0_1, npcB0_0, npcB0_2, npcB0_0 });
			aela.m_sprite.add("left_static", 0.2f, { npcB1_0});
			aela.m_sprite.add("right_static", 0.2f, { npcB2_0});
			aela.m_sprite.add("up_static", 0.2f, { npcB3_0});
			aela.m_sprite.add("down_static", 0.2f , { npcB0_0});
			aela.m_sprite.set_position(0, 552);
			bool aela_right = true;
			bool aela_stop_moving = false;
			
			/// Aela's Quotes
			sf::SoundBuffer buffer_aela_1;
			buffer_aela_1.loadFromFile("../media/sound/kurt/help_you.ogg");
			sf::Sound aela_help_you;
			aela_help_you.setBuffer(buffer_aela_1);
			aela_help_you.setVolume(m_setting.sound_volume());
			
			/// Roderick's Textures
			auto const & actorC0_0 = thoth::textures().load_extern("actorC0_0", "../media/texture/Kazzador/npc/VXActor-C/actorC_0-0.png");
			auto const & actorC0_1 = thoth::textures().load_extern("actorC0_1", "../media/texture/Kazzador/npc/VXActor-C/actorC_0-1.png");
			auto const & actorC0_2 = thoth::textures().load_extern("actorC0_2", "../media/texture/Kazzador/npc/VXActor-C/actorC_0-2.png");
			auto const & actorC1_0 = thoth::textures().load_extern("actorC1_0", "../media/texture/Kazzador/npc/VXActor-C/actorC_1-0.png");
			auto const & actorC1_1 = thoth::textures().load_extern("actorC1_1", "../media/texture/Kazzador/npc/VXActor-C/actorC_1-1.png");
			auto const & actorC1_2 = thoth::textures().load_extern("actorC1_2", "../media/texture/Kazzador/npc/VXActor-C/actorC_1-2.png");
			auto const & actorC2_0 = thoth::textures().load_extern("actorC2_0", "../media/texture/Kazzador/npc/VXActor-C/actorC_2-0.png");
			auto const & actorC2_1 = thoth::textures().load_extern("actorC2_1", "../media/texture/Kazzador/npc/VXActor-C/actorC_2-1.png");
			auto const & actorC2_2 = thoth::textures().load_extern("actorC2_2", "../media/texture/Kazzador/npc/VXActor-C/actorC_2-2.png");
			auto const & actorC3_0 = thoth::textures().load_extern("actorC3_0", "../media/texture/Kazzador/npc/VXActor-C/actorC_3-0.png");
			auto const & actorC3_1 = thoth::textures().load_extern("actorC3_1", "../media/texture/Kazzador/npc/VXActor-C/actorC_3-1.png");
			auto const & actorC3_2 = thoth::textures().load_extern("actorC3_2", "../media/texture/Kazzador/npc/VXActor-C/actorC_3-2.png");
			
			dungeon::npc roderick(actorC0_0, "Roderick");
			roderick.m_sprite.add("left", 0.2f, { actorC1_1, actorC1_0, actorC1_2, actorC1_0 });
			roderick.m_sprite.add("right", 0.2f, { actorC2_1,  actorC2_0, actorC2_2, actorC2_0 });
			roderick.m_sprite.add("up", 0.2f, { actorC3_1, actorC3_0, actorC3_2, actorC3_0 });
			roderick.m_sprite.add("down", 0.2f ,{ actorC0_1, actorC0_0, actorC0_2, actorC0_0 });
			roderick.m_sprite.add("left_static", 0.2f, { actorC1_0});
			roderick.m_sprite.add("right_static", 0.2f, { actorC2_0});
			roderick.m_sprite.add("up_static", 0.2f, { actorC3_0});
			roderick.m_sprite.add("down_static", 0.2f , { actorC0_0});
			roderick.m_sprite.set_position(272, 788);
			
			/// Roderick's Quotes
			sf::SoundBuffer buffer_roderick_1;
			buffer_roderick_1.loadFromFile("../media/sound/kurt/greating.ogg");
			sf::Sound roderick_greating;
			roderick_greating.setBuffer(buffer_roderick_1);
			roderick_greating.setVolume(m_setting.sound_volume());
			
			
			/// Necromancer's Textures
			auto const & skeleton_king0_0 = thoth::textures().load_extern("skeleton_king0_0", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_0-0.png");
			auto const & skeleton_king0_1 = thoth::textures().load_extern("skeleton_king0_1", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_0-1.png");
			auto const & skeleton_king0_2 = thoth::textures().load_extern("skeleton_king0_2", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_0-2.png");
			auto const & skeleton_king1_0 = thoth::textures().load_extern("skeleton_king1_0", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_1-0.png");
			auto const & skeleton_king1_1 = thoth::textures().load_extern("skeleton_king1_1", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_1-1.png");
			auto const & skeleton_king1_2 = thoth::textures().load_extern("skeleton_king1_2", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_1-2.png");
			auto const & skeleton_king2_0 = thoth::textures().load_extern("skeleton_king2_0", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_2-0.png");
			auto const & skeleton_king2_1 = thoth::textures().load_extern("skeleton_king2_1", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_2-1.png");
			auto const & skeleton_king2_2 = thoth::textures().load_extern("skeleton_king2_2", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_2-2.png");
			auto const & skeleton_king3_0 = thoth::textures().load_extern("skeleton_king3_0", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_3-0.png");
			auto const & skeleton_king3_1 = thoth::textures().load_extern("skeleton_king3_1", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_3-1.png");
			auto const & skeleton_king3_2 = thoth::textures().load_extern("skeleton_king3_2", "../media/texture/Kazzador/skeleton/Skeleton_King/skeleton_king_3-2.png");
			
			dungeon::npc necromancer(skeleton_king0_0, "Ulrich", 15.f, 15.f, 15.f, 15.f);
			necromancer.m_sprite.add("left", 0.2f, { actorC1_1, actorC1_0, actorC1_2, actorC1_0 });
			necromancer.m_sprite.add("right", 0.2f, { actorC2_1,  actorC2_0, actorC2_2, actorC2_0 });
			necromancer.m_sprite.add("up", 0.2f, { actorC3_1, actorC3_0, actorC3_2, actorC3_0 });
			necromancer.m_sprite.add("down", 0.2f ,{ actorC0_1, actorC0_0, actorC0_2, actorC0_0 });
			necromancer.m_sprite.add("left_static", 0.2f, { actorC1_0});
			necromancer.m_sprite.add("right_static", 0.2f, { actorC2_0});
			necromancer.m_sprite.add("up_static", 0.2f, { actorC3_0});
			necromancer.m_sprite.add("down_static", 0.2f , { actorC0_0});
			necromancer.m_sprite.set_position(275, 105);
			
			/// Bad News
			sf::SoundBuffer buffer1;
			buffer1.loadFromFile("../media/sound/Remaxim/horror_sound/excited_horror_sound.wav");
			sf::Sound dark_ambiance;
			dark_ambiance.setBuffer(buffer1);
			dark_ambiance.setVolume(m_setting.sound_volume());
			
			/// Necromancer's Quotes
			sf::SoundBuffer buffer_necromancer_1;
			buffer_necromancer_1.loadFromFile("../media/sound/Vinrax/i_see_you.ogg");
			sf::Sound necromancer_greating;
			necromancer_greating.setBuffer(buffer_necromancer_1);
			necromancer_greating.setVolume(m_setting.sound_volume());
			
			/// Necromancer's Quotes
			sf::SoundBuffer buffer_necromancer_2;
			buffer_necromancer_2.loadFromFile("../media/sound/Vinrax/scream_horror1_0.ogg");
			sf::Sound necromancer_scream;
			necromancer_scream.setBuffer(buffer_necromancer_2);
			necromancer_scream.setVolume(m_setting.sound_volume());
			
			/// Teleporter
			sf::SoundBuffer buffer;
			buffer.loadFromFile("../media/sound/Ogrebane/teleport.wav");
			sf::Sound teleport;
			teleport.setBuffer(buffer);
			teleport.setVolume(m_setting.sound_volume());
			
			bool apparition = false;
			bool necromancer_dead = false;
			
			/// Map
			std::size_t index = 0;
			
			
			auto const & map_village = thoth::textures().load_extern("village", "../media/texture/Dungeon/map/village1-1.png");
			dungeon::map map(map_village, 1280.f, 896.f);
			load_village(map);
			
			map.add_passif(aela);
			map.add_passif(roderick);
			
			dungeon::camera cam(m_setting, map);
			m_cams.push_back(cam);
			m_maps.push_back(map);
			
			auto const & map_room = thoth::textures().load_extern("room", "../media/texture/Dungeon/map/room1-p0.png");
			dungeon::map map1(map_room, 448.f, 225.f);
			load_room_test(map1);
			dungeon::camera cam1(m_setting, map1);
			m_cams.push_back(cam1);
			m_maps.push_back(map1);
			
			auto const & map_forest3 = thoth::textures().load_extern("forest3", "../media/texture/Dungeon/map/forest3-1.png");
			dungeon::map map2(map_forest3, 1280.f, 1120.f);
			load_forest_1(map2);
			dungeon::camera cam2(m_setting, map2);
			m_cams.push_back(cam2);
			m_maps.push_back(map2);
			
			auto const & map_boss = thoth::textures().load_extern("boss", "../media/texture/Dungeon/map/boss-p0.png");
			dungeon::map map3(map_boss, 544.f, 500.f);
			load_boss_room(map3);
			map3.add_hostil(necromancer);
			dungeon::camera cam3(m_setting, map3);
			m_cams.push_back(cam3);
			m_maps.push_back(map3);
			
			/// Animated sprite
			p1.get_hero().m_sprite.add("left", 0.2f, { t1_1, t1_0, t1_2, t1_0 });
			p1.get_hero().m_sprite.add("right", 0.2f, { t2_1,  t2_0, t2_2, t2_0 });
			p1.get_hero().m_sprite.add("up", 0.2f, { t3_1, t3_0, t3_2, t3_0 });
			p1.get_hero().m_sprite.add("down", 0.2f ,{ t0_1, t0_0, t0_2, t0_0 });
			p1.get_hero().m_sprite.add("left_static", 0.2f, { t1_0});
			p1.get_hero().m_sprite.add("right_static", 0.2f, { t2_0});
			p1.get_hero().m_sprite.add("up_static", 0.2f, { t3_0});
			p1.get_hero().m_sprite.add("down_static", 0.2f , { t0_0});
			p1.get_hero().m_sprite.set_position(73, 42);	
			
			p1.get_hero().m_sprite.set_position(800, 850);
			
			sf::Clock clock;
			
			sf::Music town_theme;
			town_theme.openFromFile("../media/music/Remaxim/old_city_theme/old_city_theme.ogg");
			town_theme.play();
			town_theme.setVolume(m_setting.music_volume());
			town_theme.setLoop(true);
			
			sf::Clock moving_clock;
			
			/// Start
			while (m_window.isOpen())
			{
				sf::Event event;
				while (m_window.pollEvent(event))
				{
					/// Close
					if (event.type == sf::Event::Closed)
					{
						m_window.close();
					}
					
					/// Key released
					else if (event.type == sf::Event::KeyReleased)
					{
						if (event.key.code == sf::Keyboard::Space)
						{
							m_window.close();
						}
						else if (event.key.code == sf::Keyboard::C)
						{
							auto const x = p1.get_hero().m_sprite.position().x;
							auto const y = p1.get_hero().m_sprite.position().y;
							std::cout << "m_sprite.set_position(" << x << ", " <<  y << ");" << std::endl;
							std::cout << m_maps.size() << std::endl;
						}
					}
				}
				
				/// Time
				float const elapsed = clock.restart().asSeconds();
				
				float const moving = 100 * elapsed;
				
				for(std::size_t i = 0; i < m_maps[index].passifs().size(); i++)
				{
					if (index == 0)
					{
						if (i == 0)
						{
							if (!aela_stop_moving)
							{
								if(m_maps[index].passifs().at(i).m_sprite.position().x <= 1280 && aela_right)
								{
									m_maps[index].passifs().at(i).m_sprite.animate("right");
									m_maps[index].passifs().at(i).m_sprite.move(moving, 0);
								}
								else if(m_maps[index].passifs().at(i).m_sprite.position().x >= 1280)
								{
									aela_right = false;
								}
								
								if(m_maps[index].passifs().at(i).m_sprite.position().x >= 0 && !aela_right)
								{
									m_maps[index].passifs().at(i).m_sprite.animate("left");
									m_maps[index].passifs().at(i).m_sprite.move(-moving, 0);
								}
								else if (m_maps[index].passifs().at(i).m_sprite.position().x <= 0)
								{
									aela_right = true;
								}
							}
							else if (aela_stop_moving && aela_right)
							{
								m_maps[index].passifs().at(i).m_sprite.animate("right_static");
							}
							else if (aela_stop_moving && !aela_right)
							{
								m_maps[index].passifs().at(i).m_sprite.animate("left_static");
							}
						}
					}
				}
				
				for (std::size_t i = 0; i < m_maps[index].passifs().size(); i++)
				{
					m_maps[index].passifs().at(i).m_sprite.update(elapsed);
				}
				
				/// Keyboard
				auto p1_previous_position = p1.get_hero().m_sprite.position();
				p1.move(moving);
				
				if (index == 0)
				{
					if (thoth::collision(p1.get_hero().m_sprite, m_maps[0].passifs().at(1).m_sprite))
					{
						p1.get_hero().m_sprite.set_position(p1_previous_position);
						roderick_greating.play();
					}
					
					auto aela_previous_position = aela.m_sprite.position();
					if (thoth::collision(p1.get_hero().m_sprite, m_maps[0].passifs().at(0).m_sprite))
					{
						aela_help_you.play();
						aela_stop_moving = true;
						aela.m_sprite.set_position(aela_previous_position);
						p1.get_hero().m_sprite.set_position(p1_previous_position);
						moving_clock.restart();
					}
					else
					{
						if(moving_clock.getElapsedTime() >= sf::seconds(2.f))
						{
							aela_stop_moving = false;
						}
					}
				}
				
				/// Player view
				m_cams[index].view(p1);
				
				m_maps[index].border(m_window, p1);
				
				
				if (index == 0)
				{
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[0].teleporter().at(0).m_sprite))
					{
						index = 1;
						p1.get_hero().m_sprite.set_position(225, 170);	
					}
					
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[0].teleporter().at(1).m_sprite))
					{
						index = 2;
						p1.get_hero().m_sprite.set_position(936, 85);	
					}
				}
				
				if (index == 1)
				{
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[1].teleporter().at(0).m_sprite))
					{
						index = 0;
						p1.get_hero().m_sprite.set_position(977, 835);	
					}
				}
				if (index == 2)
				{
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[2].teleporter().at(0).m_sprite))
					{
						index = 0;
						p1.get_hero().m_sprite.set_position(1230, 552);	
					}
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[2].teleporter().at(1).m_sprite))
					{
						town_theme.pause();
						if (!necromancer_dead)
						{
							dark_ambiance.play();
							necromancer_greating.play();
						}
						index = 3;
						p1.get_hero().m_sprite.set_position(275, 450);	
					}
				}
				if (index == 3)
				{
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[3].teleporter().at(0).m_sprite))
					{
						town_theme.play();
						index = 2;
						p1.get_hero().m_sprite.set_position(290, 550);	
					}
					
					if(p1.get_hero().m_sprite.position().y <= 320 && !apparition)
					{
						apparition = true;
						teleport.play();
					}
				}
				/// Combat
				for(int i = 0; i < m_maps[index].hostils().size(); i++)
				{
					if (thoth::collision(p1.get_hero().m_sprite, m_maps[index].hostils().at(i).m_sprite))
					{
						if(index == 3 && i == 0)
						{
							palindor_quote_1.play();
							hnc::sleep::s(2);
							palindor_quote_2.play();
							hnc::sleep::s(2);
							
							if(dungeon::combat::fight(m_window, m_setting, p1.get_hero(), m_maps[index].hostils().at(i)))
							{
								necromancer_dead = true;
								necromancer_scream.play();
								hnc::sleep::s(8);
								m_maps[index].hostils().erase(m_maps[index].hostils().begin()+i);
								teleport.play();
								hnc::sleep::s(2);
								palindor_quote_3.play();
								
							}
							else
							{
								p1.get_hero().m_sprite.set_position(800, 850);
								p1.get_hero().set_mana(100);
								p1.get_hero().set_life(100);
								town_theme.play();
							}
						}
						else
						{
							town_theme.pause();
							if(dungeon::combat::fight(m_window, m_setting, p1.get_hero(), m_maps[index].hostils().at(i)))
							{
								m_maps[index].hostils().erase(m_maps[index].hostils().begin()+i);
								town_theme.play();
							}
							else
							{
								index = 0;
								p1.get_hero().m_sprite.set_position(800, 850);
								p1.get_hero().set_mana(100);
								p1.get_hero().set_life(100);
								town_theme.play();
							}
						}
					}
				}
				
				
				/// Collisions
				for (int i = 0; i < m_maps[index].miscs().size(); i++)
				{
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[index].miscs().at(i).m_sprite))
					{
						p1.get_hero().m_sprite.set_position(p1_previous_position);
					}
				}
				
				for (int i = 0; i < m_maps[index].blocks().size(); i++)
				{
					if(thoth::collision(p1.get_hero().m_sprite, m_maps[index].blocks().at(i).m_sprite))
					{
						p1.get_hero().m_sprite.set_position(p1_previous_position);
					}
				}
				
				/// Clear the screen
				m_window.clear(sf::Color::Black);
				
				/// Draw Map's Background
				m_maps[index].draw(m_window);
				
				/// Draw Teleporters
				for (auto const & sprite : m_maps[index].teleporter())
				{
					m_window << sprite;
				}
				
				/// Draw Miscs
				for (auto const & sprite : m_maps[index].miscs())
				{
					m_window << sprite;
				}
				
				/// Draw Statics
				for (auto const & sprite : m_maps[index].statics())
				{
					m_window << sprite;
				}
				
				/// Drawn Palindor
				p1.get_hero().m_sprite.update(elapsed);
				m_window << p1;
				
				/// Draw Hostils
				for (std::size_t i = 0; i < m_maps[index].hostils().size(); i++)
				{
					if(index == 3)
					{
						if(apparition)
						{
							m_window << m_maps[index].hostils().at(i);
						}
					}
					else
					{
						m_window << m_maps[index].hostils().at(i);
					}
				}
				
				/// Draw Passifs
				for (auto const & sprite : m_maps[index].passifs())
				{
					m_window << sprite;
				}
				
				m_window.setView(m_cams[index].getView());
				
				/// Draw Cursor
				sf::Vector2i pixelPos = sf::Mouse::getPosition(m_window);
				sf::Vector2f worldPos = m_window.mapPixelToCoords(pixelPos);
				m_setting.cursor().set_position(worldPos.x, worldPos.y);
				m_window << m_setting.cursor();
				
				
				/// Display	
				m_window.display();
				
				
			}
		}
		
		sf::RenderWindow & window()
		{
			return m_window;
		}
		
		dungeon::setting & setting()
		{
			return m_setting;
		}
		
	};

}
#endif 
