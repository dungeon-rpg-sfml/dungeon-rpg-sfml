// Copyright © 2014 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DUNGEON_RPG_SFML_NPC_HPP
#define DUNGEON_RPG_SFML_NPC_HPP

#include <hnc/serialization.hpp>

#include <SFML/Graphics.hpp>

#include <thoth/textures.hpp>
#include <thoth/sprite_animated.hpp>


namespace dungeon 
{
	/**
	 * @brief NPC
	 * 
	 * @code
	   #include <thoth/sprite_animated.hpp>
	   #include <thoth/texture_management.hpp>
	   @endcode
	 * 
	 */
	
	class npc 
	{
	private:
		
		std::string m_name;
		
		thoth::texture m_default_texture;
		
		int m_life = 100;
		
		int m_mana = 100;
		
		/// Strength of the NPC
		float m_strength;
		
		/// Adress of the NPC
		float m_adress;
		
		/// Intelligence of the NPC
		float m_intelligence;
		
		/// Luck of the NPC
		float m_luck;
		
		int m_damage;
		
		/// Skills
		float m_one_handed = 10.f;
		float m_two_handed = 10.f;
		float m_block = 10.f;
		float m_ligth_armor = 10.f;
		float m_heavy_armor = 10.f;
		
    
	public:
		
        /// Animated sprite of the NPC
		thoth::sprite_animated m_sprite;
		
		/// @brief Constructor
		/// @param[in] default_texture Default texture of the NPC's sprite
		npc(thoth::texture const & default_texture, std::string const & name) : m_default_texture (default_texture),
		m_sprite(m_default_texture),
		m_name (name)
		{
			m_strength = 10.f;
			m_adress = 10.f;
			m_intelligence = 10.f;
			m_luck = 10.f;
			m_damage = 8;
		} 	
		
		/// @brief Constructor
		/// @param[in] default_texture Default texture of the NPC's sprite
		/// @param[in] strength		   Strength value of the NPC
		/// @param[in] adress	       Adress value of the NPC
		/// @param[in] intelligence    Intelligence value of the NPC
		/// @param[in] luck	           Luck value of the NPC
		npc(thoth::texture const & default_texture, std::string const & name, int const & strength, int const & adress, int const & intelligence, int const & luck) : m_default_texture (default_texture),
		m_sprite(default_texture),
		m_name (name)
		{
			m_strength = strength;
			m_adress = adress;
			m_intelligence = intelligence;
			m_luck = luck;
			m_damage = 8;
		} 	
		
		thoth::texture & default_texture()
		{
			return m_default_texture;
		}
		
		std::string & name()
		{
			return m_name;
		}
		
		/// GENERAL STATS
		
		/// @brief Return the NPC's life
		/// @return m_strength
		float life() 
		{
			return m_life;
		}	
		
		/// @brief Return the NPC's life
		/// @return m_strength
		float mana() 
		{
			return m_mana;
		}	
		
		/// @brief Return the NPC's strength
		/// @return m_strength
		float damage() 
		{
			return m_damage;
		}	
		
		/// @brief Return the NPC's strength
		/// @return m_strength
		float strength() 
		{
			return m_strength;
		}	
		
		/// @brief Return the NPC's Adress
		/// @return m_adress
		float adress() 
		{
			return m_adress;
		}
		
		/// @brief Return the NPC's Intelligence
		/// @return m_intelligence
		float intelligence() 
		{
			return m_intelligence;
		}
		
		/// @brief Return the NPC's Luck
		/// @return m_luck
		float luck() 
		{
			return m_luck;
		}
		
		/// @brief Set the Life value
		/// @param[in] life
		void set_life(float life)
		{
			m_life = life;
		}
		
		/// @brief Set the Life value
		/// @param[in] life
		void set_mana(float mana)
		{
			m_mana = mana;
		}
		
		/// @brief Set the Damage value
		/// @param[in] damage
		void set_damage(float damage)
		{
			m_damage = damage;
		}
		
		/// @brief Set the Strength value
		/// @param[in] strength
		void set_strength(float strength)
		{
			m_strength = strength;
		}	
		
		/// @brief Set the Adress value
		/// @param[in] adress
		void set_adress(float adress)
		{
			m_adress = adress;
		}
		
		/// @brief Set the Intelligence value
		/// @param[in] intelligence
		void set_intelligence(float intelligence)
		{
			m_intelligence = intelligence;
		}
		
		/// @brief Set the Luck value
		/// @param[in] luck
		void set_luck(float luck)
		{
			m_luck = luck;
		}
		
		
		/// SECONDARY SKILLS
		
		/// @brief Return the NPC's block
		/// @return m_strength
		float block() 
		{
			return m_block;
		}	
		
		void set_block(float const & block)
		{
			m_block = block;
		}
	};
	
	
	sf::RenderWindow & operator<<(sf::RenderWindow & window, dungeon::npc const & npc)
	{
		window << npc.m_sprite;
		return  window;
	}

}

#endif
